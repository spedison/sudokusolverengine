package br.com.sudoku.view;

import br.com.sudoku.kernel.interfaces.Consts;
import br.com.sudoku.kernel.solvers.GameStrategy;
import br.com.sudoku.kernel.vo.Cell;
import br.com.sudoku.kernel.vo.groupelements.GroupBase;

/**
 * Created by @GrupoAlpha on 11/06/15.
 */
public class KeyControl {

    private StringBuffer out;
    private String tableName;
    private GameStrategy gameStrategy;

    public KeyControl() {
        tableName = "";
        out = new StringBuffer();
    }

    public void setGameStrategy(GameStrategy gameStrategy) {
        this.gameStrategy = gameStrategy;
    }


    public String writeKeyControlKeys(String tableName, String sessionId) {

        this.tableName = tableName;

        out.setLength(0);

        this.tableName = tableName;

        for (int row = 0; row < Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME; row++) {
            GroupBase rowCell = gameStrategy.getGame().getRow(row);
            for (int col = 0; col < Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME; col++) {

                Cell cell = rowCell.get(col);

                if (cell.hasValue() || cell.isFixedValue())
                    continue;

                String controlName = tableName + "_ED_" + row + "" + col;
                writeVariable(controlName, cell.getPossibitiesString());
                writeFunction(controlName);
                writeAssingControl(controlName);
                out.append("\n\n");
            }
        }

        writeResponseFunction(sessionId);

        return out.toString();
    }

    private void writeResponseFunction(String sessionId) {
        out.append(
                "function sendValue (controlName, valueToSend, trick){\n" +
                        "   $.get(\"./sudoku.cmd?cmd=key&session=" + sessionId + "&value=\" + valueToSend + \"&control=\" + controlName + \"&trick=\" + trick ,\"\", function(data, status, jq){processReceivedData(data);});\n" +
                        "}\n"

        );
    }

    private void writeAssingControl(String controlName) {

        String stringFormat = "\t\t$(\"#%s\").keyup(FUNC_KEY_RESPONSE_%s);\n";
        out.append(String.format(stringFormat, controlName, controlName));
    }

    private void writeFunction(String controlName) {

        String strFunction =
                "function FUNC_KEY_RESPONSE_" + controlName + "(){\n\n" +
                        "   if ($(\"#" + controlName + "\").prop(\'readOnly\') == true) {return;}\n" +

                        "   var controle = $(\"#" + controlName + "\").val();\n" +
                        "\n" +
                        "   if(controle == null || controle == \"\") return ;\n" +
                        "\n" +

                        "   if(KEY_LIMIT_" + controlName + ".indexOf(controle) == -1){\n" +
                        "       $(\"#" + controlName + "\").val(\"\");\n" +
                        "       $(\"#result\").val(\"Tecla não permitida.\");\n" +
                        "   }\n" +
                        "   else {\n" +
                        "       sendValue(\"" + controlName + "\" ,controle, true);\n" +
                        "   }\n" +
                        "}\n";

        out.append(strFunction);
    }

    private void writeVariable(String controlName, String possibilities) {

        out.append(String.format("\t\tvar KEY_LIMIT_%s = \"%s\";\n", controlName, possibilities));
    }
}
