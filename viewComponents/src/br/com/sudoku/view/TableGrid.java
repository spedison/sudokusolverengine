package br.com.sudoku.view;

import br.com.sudoku.kernel.interfaces.Consts;
import br.com.sudoku.kernel.solvers.GameStrategy;

/**
 * Created by @GrupoAlpha on 04/06/15.
 */
public class TableGrid {

    private StringBuffer out;
    private int col;
    private int row;
    private int maxGroup;
    private String tableName;
    private GameStrategy gameStrategy;
    private boolean changeGroup;
    private boolean compacted;
    private String tabLevel;

    public TableGrid() {
        out = new StringBuffer();
        row = col = 0;
        maxGroup = 3;
        changeGroup = true;
        compacted = false;
        tableName = "";
        tabLevel = "";

    }

    private void addTab() {
        tabLevel += "    ";
    }

    private void removeTab() {

        if (tabLevel.isEmpty())
            return;

        tabLevel = tabLevel.substring(4);
    }

    private void resetGrid() {
        out.setLength(0);
        row = col = 0;
        tabLevel = "";
        changeGroup = true;
        tableName = "";
    }

    public void setGameStrategy(GameStrategy gameStrategy) {
        this.gameStrategy = gameStrategy;
    }

    private void writeLine(String line) {

        if (!compacted)
            out.append(tabLevel);

        out.append(line);

        if (compacted) return;

        out.append("\n");
    }


    private void writeHeader1() {
        writeLine("<table cellspacing=\"0\" cellpadding=\"0\">");
        addTab();
        writeLine("<tbody>");
    }

    private void writeTail1() {
        writeLine("</tbody>");
        removeTab();
        writeLine("</table>");
    }

    private void writeHeader2() {

        if (changeGroup)
            writeLine("<td class=\"boardCellGroupA\">");
        else
            writeLine("<td class=\"boardCellGroupB\">");

        changeGroup = !changeGroup;

        addTab();
        writeLine("<table cellspacing=\"1\" cellpadding=\"0\">");

        addTab();
        writeLine("<tbody>");
    }

    private void writeTail2() {
        writeLine("</tbody>");

        removeTab();
        writeLine("</table>");

        removeTab();
        writeLine("</td>");
    }

    private void startGroup() {
        writeLine("<tr>");
    }

    private void endGroup() {
        writeLine("</tr>");
    }

    private void writeItem() {
        if (gameStrategy.getGame().getRow(row).get(col).isFixedValue() && gameStrategy.getGame().getRow(row).get(col).hasValue()) {
            writeLine("<td class=\"boardCell\">");

            addTab();
            writeLine("<div id=\"" + tableName + "_" + row + "" + col + "\" class=\"staticValue\"><span>" + Consts.SIMBOLS[gameStrategy.getGame().getRow(row).get(col).getValue()] + "</span></div>");
            removeTab();

            writeLine("</td>");
        } else {
            writeLine("<td class=\"boardCell\">");

            addTab();
            writeLine("<div id=\"" + tableName + "_" + row + "" + col + "\" class=\"editValue\"><input id=\"" + tableName + "_ED_" + row + "" + col + "\" maxlength=\"1\"/></div>");
            removeTab();

            writeLine("</td>");
        }
    }

    private void writeHeaderGroup() {
        writeLine("<tr>");
    }

    private void writeTailGroup() {
        writeLine("</tr>");
    }

    public String writeTable(String tableName) {

        resetGrid();

        this.tableName = tableName;

        addTab();
        writeHeader1();

        for (int groupRow = 0; groupRow < maxGroup; groupRow++) {

            addTab();
            startGroup();

            int offRow = groupRow * maxGroup;

            for (int groupCol = 0; groupCol < maxGroup; groupCol++) {

                addTab();
                int offCol = groupCol * maxGroup;
                writeHeader2();

                for (int itemRow = 0; itemRow < maxGroup; itemRow++) {

                    addTab();
                    writeHeaderGroup();
                    row = itemRow + offRow;

                    for (int itemCol = 0; itemCol < maxGroup; itemCol++) {
                        addTab();
                        col = itemCol + offCol;
                        writeItem();
                        removeTab();
                    }

                    writeTailGroup();
                    removeTab();
                }

                writeTail2();
                removeTab();
            }

            endGroup();
            removeTab();

        }

        writeTail1();
        return out.toString();
    }
}
