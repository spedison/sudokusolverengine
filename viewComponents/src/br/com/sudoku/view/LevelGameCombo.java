package br.com.sudoku.view;

import br.com.sudoku.kernel.interfaces.LevelGameType;

/**
 * File LevelGameCombo.java created by @GrupoAlpha on 11/06/2015 at 09:36 for projetct SudokuSolverEngine.
 */
public class LevelGameCombo {


    static public String makeCombo(String levelSelected, String id, String name) {

        StringBuilder out = new StringBuilder();

        out.append("<select class=\"form-control\" name=\""+ name +"\" id=\"" + id + "\">\n");
        for (LevelGameType t : LevelGameType.values()) {
            out.append(String.format("<option %s value=\"%s\">%s</option>\n",
                    t.name().equalsIgnoreCase(levelSelected) ? "SELECTED" : "",
                    t.name(),
                    t.getPrettyName()));
        }

        out.append("</select>\n");
        return out.toString();
    }


}
