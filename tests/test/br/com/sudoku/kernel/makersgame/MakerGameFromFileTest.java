package br.com.sudoku.kernel.makersgame;

import br.com.sudoku.kernel.interfaces.LevelGameType;
import br.com.sudoku.kernel.vo.Game;
import br.com.sudoku.kernel.interfaces.MakerGameInterface;
import br.com.sudoku.kernel.solvers.GameStrategy;
import br.com.sudoku.kernel.printers.PrinterGame;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by @GrupoAlpha on 02/06/15.
 */
public class MakerGameFromFileTest {

    MakerGameInterface makerGame;
    PrinterGame printerGame;

    @Before
    public void setUp() throws Exception {

        makerGame = new MakerGameFromFile();
        printerGame = new PrinterGame();

        System.out.println("\n\nProcessando MakerGameFromFileTest\n\n");
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testMakeGame() throws Exception {

        GameStrategy gameStrategy = new GameStrategy();
        Game game = new Game();
        gameStrategy.setGame(game);

        makerGame.makeGame(gameStrategy, LevelGameType.VERY_EASY);

        Assert.assertFalse("Jogo terminado ? Como assim ??", gameStrategy.endOfGame());

        printerGame.print(game);

    }
}