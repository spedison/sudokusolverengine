package br.com.sudoku.kernel.loaders;

import br.com.sudoku.kernel.solvers.GameStrategy;
import br.com.sudoku.kernel.loadersgame.LoaderGameFileTextResolved;
import br.com.sudoku.kernel.vo.Game;
import br.com.sudoku.kernel.printers.PrinterGame;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * File LoaderGameFileTextResolvedTest.java created by @GrupoAlpha on 29/05/2015 at 19:59 for projetct SudokuSolverEngine.
 */
public class LoaderGameFileTextResolvedTest {

    private Game game;
    private LoaderGameFileTextResolved loader;
    private PrinterGame printer;

    public LoaderGameFileTextResolvedTest() {
        game = new Game();
        printer = new PrinterGame();
        loader = new LoaderGameFileTextResolved();
    }

    @Before
    public void setUp() throws Exception {
        System.out.println("\n\nProcessando LoaderGameFileTextResolvedTest\n\n");
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void loadFile() throws Exception {

        org.junit.Assert.assertTrue(loader.load(game));

        GameStrategy gameStrategy = new GameStrategy();
        gameStrategy.setGame(game);

        org.junit.Assert.assertTrue(gameStrategy.endOfGame());
        printer.print(game);

    }
}