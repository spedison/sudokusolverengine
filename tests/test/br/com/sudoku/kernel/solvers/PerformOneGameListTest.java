package br.com.sudoku.kernel.solvers;

import br.com.sudoku.kernel.interfaces.Consts;
import br.com.sudoku.kernel.interfaces.StepExecute;
import br.com.sudoku.kernel.loadersgame.LoaderGameFileTextResolved;
import br.com.sudoku.kernel.printers.PrinterGame;
import br.com.sudoku.kernel.vo.Game;
import br.com.sudoku.kernel.vo.Position;
import br.com.sudoku.kernel.vo.results.Solution;
import br.com.sudoku.kernel.vo.results.SolutionList;
import br.com.sudoku.kernel.vo.results.Step;
import br.com.sudoku.utils.ConfigEnviroment;
import br.com.sudoku.utils.LoadLog4J;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * File LoaderGameFileTextResolvedTest.java created by @GrupoAlpha on 29/05/2015 at 19:59 for projetct SudokuSolverEngine.
 */
public class PerformOneGameListTest implements StepExecute {

    private Game game;
    private LoaderGameFileTextResolved loader;
    private PrinterGame printer;

    public PerformOneGameListTest() {

        LoadLog4J.LoadFromFile(ConfigEnviroment.getHomeDir() +
                File.separator + "git" +
                File.separator + "sudokusolverengine" +
                File.separator + "files" +
                File.separator + "log4j.xml");

        game = new Game();
        printer = new PrinterGame();
        loader = new LoaderGameFileTextResolved();

    }

    @Before
    public void setUp() throws Exception {

        System.out.println("\n\nProcessando PerformOneGameListTest\n\n");

        List<Position> positionList = new LinkedList<>();
        Game filled = new Game();
        loader.load(filled, 10);
        Random rand = new Random(System.currentTimeMillis());

        for (int x = 0; x < 35; x++) {
            Position pos = new Position(rand.nextInt(Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME),
                    rand.nextInt(Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME));

            positionList.add(pos);
        }

        GameStrategy gm = new GameStrategy();
        gm.setGame(filled);
        if (gm.endOfGame()) {
            gm.setGame(game);
            gm.copy(filled, positionList);
        } else {
            game = filled;
            gm.setGame(game);
        }
        printer.print(game);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Override
    public void processedOneStep(Step step, Game game) {
        System.out.printf("\n\nColocado [%d,%d]= %s\n", step.getPosition().getRow(), step.getPosition().getCol(), Consts.SIMBOLS[step.getValue()]);
        printer.print(game);
    }

    @Test
    public void solveGame() throws Exception {

        GameStrategy gameStrategy = new GameStrategy();

        PerformOneGameList performOneGameList = new PerformOneGameList();
        performOneGameList.setGameStrategy(new GameStrategy());
        performOneGameList.setStepInterface(null);

        gameStrategy.setGame(game);

        org.junit.Assert.assertFalse(gameStrategy.endOfGame());

        SolutionList solutionList = new SolutionList();

        performOneGameList.setMaxSolutions(4);
        performOneGameList.setStepInterface(this);
        performOneGameList.execute(game, solutionList);

        int conta = 0;
        for (Solution solution : solutionList) {
            System.out.println("Resultado do Jogo #" + (conta++) + "/" + solutionList.size() );
            printer.print(solution.getGame());
        }
    }
}