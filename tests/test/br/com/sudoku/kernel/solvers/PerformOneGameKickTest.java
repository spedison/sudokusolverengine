package br.com.sudoku.kernel.solvers;

import br.com.sudoku.kernel.interfaces.Consts;
import br.com.sudoku.kernel.loadersgame.LoaderGameFileTextResolved;
import br.com.sudoku.kernel.vo.Game;
import br.com.sudoku.kernel.vo.Position;
import br.com.sudoku.kernel.vo.results.Solution;
import br.com.sudoku.kernel.printers.PrinterGame;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * File LoaderGameFileTextResolvedTest.java created by @GrupoAlpha on 29/05/2015 at 19:59 for projetct SudokuSolverEngine.
 */
public class PerformOneGameKickTest {

    private Game game;
    private LoaderGameFileTextResolved loader;
    private PrinterGame printer;

    public PerformOneGameKickTest() {
        game = new Game();
        printer = new PrinterGame();
        loader = new LoaderGameFileTextResolved();
    }

    @Before
    public void setUp() throws Exception {

        System.out.println("\n\nProcessando PerformOneGameKick\n\n");

        List<Position> positionList = new LinkedList<>();
        Game filled = new Game();
        loader.load(filled);
        Random rand = new Random(System.currentTimeMillis());


        for (int x = 0; x < 35; x++) {
            Position pos = new Position(rand.nextInt(Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME),
                    rand.nextInt(Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME));

            positionList.add(pos);
        }

        GameStrategy gm = new GameStrategy();
        gm.setGame(game);
        gm.copy(filled, positionList);
        System.out.println("\n\n\nAntes de processar:");
        printer.print(game);

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void solveGame() throws Exception {

        GameStrategy gameStrategy = new GameStrategy();

        PerformOneGameKick performOneGameKick = new PerformOneGameKick();
        performOneGameKick.setGameStrategy(new GameStrategy());
        performOneGameKick.setStepInterface(null);

        gameStrategy.setGame(game);
        org.junit.Assert.assertFalse(gameStrategy.endOfGame());

        Solution s = new Solution();

        performOneGameKick.execute(game, s);
        gameStrategy.setGame(game);
        org.junit.Assert.assertTrue(gameStrategy.endOfGame());

        System.out.println("\n\n\nDepois de processar:");
        printer.print(game);

    }
}