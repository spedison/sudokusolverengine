package br.com.sudoku.json;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by @GrupoAlpha on 23/5/15.
 */
public class JsonTest {

    static public class User implements Serializable {

        public User() {
            name = "Edison";
            idade = 38;
            mensagens = new LinkedList<>();
            mensagens.add("Número 1");
            mensagens.add("2");
            mensagens.add("Terceiro");
            mensagens.add("Quatro + 0");
            mensagens.add("Quinto!!!");
        }

        private String name;
        private int idade;
        List<String> mensagens;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getIdade() {
            return idade;
        }

        public void setIdade(int idade) {
            this.idade = idade;
        }

        public List<String> getMensagens() {
            return mensagens;
        }

        public void setMensagens(List<String> mensagens) {
            this.mensagens = mensagens;
        }
    }

    @Before
    public void before() {
        System.out.println("\n\nProcessando JsonTest\n\n");
    }

    @After
    public void after() {

    }

    @Test
    public void main() throws Exception {
        JsonTest.User user = new User();
        String fileName = System.getProperty("user.home") + "/user.json";
        {
            //1. Convert Java object to JSON format
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(new File(fileName), user);
            System.out.println(mapper.writeValueAsString(user));
            Assert.assertTrue(true);
        }

        {//2. Convert JSON to Java object
            ObjectMapper mapper = new ObjectMapper();
            User user1 = mapper.readValue(new File(fileName), JsonTest.User.class);
            Assert.assertTrue(user1.getName().equals("Edison") && user1.getIdade() == 38);
        }
    }
}
