package br.com.sudoku.view;

import br.com.sudoku.kernel.solvers.GameStrategy;
import br.com.sudoku.kernel.vo.Game;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by @GrupoAlpha on 04/06/15.
 */
public class TableGridTest {

    Game game;

    @Before
    public void setUp() throws Exception {

        game = new Game();
        game.getRow(0).get(0).setValue(1);
        game.getRow(0).get(0).setFixedValue(true);

        game.getRow(1).get(1).setValue(1);
        game.getRow(1).get(1).setFixedValue(true);

        game.getRow(2).get(2).setValue(1);
        game.getRow(2).get(2).setFixedValue(true);

        game.getRow(3).get(3).setValue(1);
        game.getRow(3).get(3).setFixedValue(true);

        game.getRow(4).get(4).setValue(1);
        game.getRow(4).get(4).setFixedValue(true);

        game.getRow(5).get(5).setValue(1);
        game.getRow(5).get(5).setFixedValue(true);

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testWriteTable() throws Exception {

        TableGrid tableGrid = new TableGrid();
        tableGrid.setGameStrategy(new GameStrategy(game));

        String ret = tableGrid.writeTable("TAB1");

        System.out.println(ret);

        // Assert.assertTrue(true);

    }
}