/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sudoku.kerneltest.game;

import br.com.sudoku.kernel.interfaces.StepExecute;
import br.com.sudoku.kernel.vo.Game;
import br.com.sudoku.kernel.vo.results.Step;

import java.util.List;


/**
 * @author @GrupoAlpha
 */
public class MainVOne {


    //static public PrinterGame printer = new PrinterGame();

    static class AddSteps implements StepExecute {

        List<Step> listaPassos;
        int pos;

        public AddSteps(List<Step> lista) {
            listaPassos = lista;
            pos = 1;
        }

        @Override
        public void processedOneStep(Step step, Game game) {

            step.setStep(pos++);
            listaPassos.add(step);
            System.out.println("Realizando a jogada em [" + (step.getPosition().getRow() + 1) + "," + (step.getPosition().getCol() + 1) + "] = " + (step.getValue() + 1));
            //printer.print(game);

        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
/*
        Game game = new Game();
        Solution solution = new Solution();

        List<Step> steps = new LinkedList<>();
        AddSteps addSteps = new AddSteps(steps);

        System.out.println("Before load");
        printer.print(game);

        String className = args[0];
        if (className == null) {
            className = "br.com.sudoku.kernel.loaders.LoaderFileTextToSolve";
        }

        if (!className.startsWith("br.com.sudoku.kernel.solvers")) {
            className = "br.com.sudoku.kernel.loaders." + className;
        }

        // Load Game
        LoaderGameInterface loader;
        try {
            loader = (LoaderGameInterface) Class.forName(className).newInstance();
        } catch (ClassNotFoundException ex) {
            System.out.println("Class not found-1.");
            ex.printStackTrace();
            return;
        } catch (InstantiationException ex) {
            System.out.println("Class not found-2.");
            ex.printStackTrace();
            return;
        } catch (IllegalAccessException ex) {
            System.out.println("Class not found-3.");
            ex.printStackTrace();
            return;
        }


        if (!loader.load(game)) {
            System.out.println("Problems while load game.");
        }

        System.out.println("After load");
        printer.print(game);


        className = args[1];
        if (className == null) {
            className = "br.com.sudoku.kernel.solvers.PerformOneGameKick";
        }

        if (!className.startsWith("br.com.sudoku.kernel.solvers")) {
            className = "br.com.sudoku.kernel.solvers." + className;
        }

        // Load Solver Game
        PerformGameInterface perform;

        try {
            perform = (PerformGameInterface) Class.forName(className).newInstance();
        } catch (ClassNotFoundException ex) {
            System.out.println("Class not found-1.");
            ex.printStackTrace();
            return;
        } catch (InstantiationException ex) {
            System.out.println("Class not found-2.");
            ex.printStackTrace();
            return;
        } catch (IllegalAccessException ex) {
            System.out.println("Class not found-3.");
            ex.printStackTrace();
            return;
        }

        perform.setStepInterface(addSteps);
        perform.setGameStrategy(new GameStrategy());
        System.out.println("Before perform");
        List<Solution> solutionList = new LinkedList<>();
        perform.execute(game, solutionList);
        //perform.execute(game, solution);

        // Show result. 

        solutionList.forEach(s -> {
            System.out.println("----- After perform START GAME ------");
            printer.print(s.getGame());
            //System.out.println("----- STEPS ------");
            //s.getStepList().
            //        stream().map(step -> String.format("Step %d write value [%s] in position [%d,%d]", step.getStep(),
            //        Consts.SIMBOLS[step.getValue()], step.getPosition().getRow(), step.getPosition().getCol())).
            //        forEach(System.out::println);
            //System.out.println("----- After perform END GAME ------");
        });

*/

    }
}
