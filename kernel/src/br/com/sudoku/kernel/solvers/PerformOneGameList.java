/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sudoku.kernel.solvers;

import br.com.sudoku.kernel.cleanstrateges.ExecuteNakedPair;
import br.com.sudoku.kernel.interfaces.Consts;
import br.com.sudoku.kernel.interfaces.GameStrategyAbstract;
import br.com.sudoku.kernel.interfaces.PerformGameInterface;
import br.com.sudoku.kernel.interfaces.StepExecute;
import br.com.sudoku.kernel.printers.PrinterGame;
import br.com.sudoku.kernel.vo.Cell;
import br.com.sudoku.kernel.vo.Game;
import br.com.sudoku.kernel.vo.Position;
import br.com.sudoku.kernel.vo.results.BoxResult;
import br.com.sudoku.kernel.vo.results.Solution;
import br.com.sudoku.kernel.vo.results.SolutionList;
import br.com.sudoku.kernel.vo.results.Step;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Stack;

/**
 * @author @GrupoAlpha
 */
public class PerformOneGameList implements PerformGameInterface {

    private StepExecute stepExecuteInterface;
    private GameStrategyAbstract gameStrategy;
    private Stack<BackupGame> backupGameStack;

    private Solution currentSolutionGame;
    private SolutionList currentSolutionGameList;
    private int maxSolutions;
    private ExecuteNakedPair executeNakedPair;

    static private Logger logger = Logger.getLogger(PerformOneGameList.class);

    PrinterGame printerGame;

    public PerformOneGameList() {
        stepExecuteInterface = null;
        backupGameStack = new Stack<>();
        gameStrategy = new GameStrategy();
        executeNakedPair = new ExecuteNakedPair();
        printerGame = new PrinterGame();
    }

    static private class BackupGame {

        private Solution solution;
        private BoxResult boxResult;

        public BackupGame(Solution solution, BoxResult boxResult) {
            this.solution = solution;
            this.boxResult = boxResult;
        }

        public Solution getSolution() {
            return solution;
        }

        public void setSolution(Solution solution) {
            this.solution = solution;
        }

        public BoxResult getBoxResult() {
            return boxResult;
        }

        public void setBoxResult(BoxResult boxResult) {
            this.boxResult = boxResult;
        }
    }


    public boolean verifyBasicCondition(GameStrategyAbstract game, BoxResult boxResult) {
        return game.canApplyResultBox(boxResult)
                && game.canApplyResultCol(boxResult)
                && game.canApplyResultRow(boxResult);
    }

    @Override
    public void execute(Solution solution) {
        currentSolutionGame = solution;
        execute();
    }

    public void execute() {

        GameStrategy gameStrategyLocal = new GameStrategy();

        BoxResult boxResult = new BoxResult();
        gameStrategyLocal.setGame(currentSolutionGame.getGame());

        do {

            boolean canExecute = false;
            gameStrategyLocal.setGame(currentSolutionGame.getGame());
            printerGame.print(gameStrategyLocal.getGame());

            // Clean game using Naked Pair Stratege.
            executeNakedPair.processGame(currentSolutionGame.getGame());

            canExecute = gameStrategyLocal.findUniqueValue(boxResult);
            if (canExecute) {
                canExecute &= verifyBasicCondition(gameStrategyLocal, boxResult);
                if (canExecute) {
                    logger.debug("Find unique value is valid!");
                    printerGame.print(gameStrategyLocal.getGame());
                }
            }

            if (!canExecute) {
                canExecute = gameStrategyLocal.getUniqueValueRow(boxResult);
                if (canExecute) {
                    canExecute &= verifyBasicCondition(gameStrategyLocal, boxResult);

                    if (canExecute) {
                        logger.debug("Unique ValueRow is valid!");
                        printerGame.print(gameStrategyLocal.getGame());
                    }
                }
            }

            if (!canExecute) {

                canExecute = gameStrategyLocal.getUniqueValueCol(boxResult);
                if (canExecute)
                    canExecute &= verifyBasicCondition(gameStrategyLocal, boxResult);

                if (canExecute) {
                    logger.debug("Unique ValueCol is valid!");
                    printerGame.print(gameStrategyLocal.getGame());
                }
            }

            if (!canExecute) {
                canExecute = gameStrategyLocal.getUniqueValueBox(boxResult);
                if (canExecute) {
                    canExecute &= verifyBasicCondition(gameStrategyLocal, boxResult);
                    if (canExecute) {
                        logger.debug("Unique ValueBox is valid!");
                        printerGame.print(gameStrategyLocal.getGame());
                    }
                }
            }


            if (canExecute) {
                // Write the solution.
                gameStrategyLocal.setGame(currentSolutionGame.getGame());
                gameStrategyLocal.writeResult(boxResult, currentSolutionGame);
                printerGame.print(gameStrategyLocal.getGame());
            } else {
                // Make new attempts...
                makePathOfGame();
                restoreLastAttempt();
                continue;
            }

            if (gameStrategyLocal.endOfGame()) {
                currentSolutionGameList.add(currentSolutionGame);
                restoreLastAttempt();
            }

            // Lets back-tracking...
            if (gameStrategyLocal.errorInGame() && !gameStrategyLocal.endOfGame()) {
                    restoreLastAttempt();
            }

            if (stepExecuteInterface != null && canExecute)
                if(currentSolutionGame != null)
                    stepExecuteInterface.processedOneStep(new Step(0, boxResult.getPos().clone(), boxResult.getValue()),
                        currentSolutionGame.getGame());

        }
        while (currentSolutionGame != null && currentSolutionGameList.size() < getMaxSolution());


    }

    private void restoreLastAttempt() {

        currentSolutionGame = null;

        if (backupGameStack.isEmpty())
            return;

        BackupGame backupGame = backupGameStack.pop();
        currentSolutionGame = backupGame.getSolution();
        gameStrategy.setGame(currentSolutionGame.getGame());
        if (!gameStrategy.writeResult(backupGame.getBoxResult(), currentSolutionGame) && !backupGameStack.isEmpty()) {
            restoreLastAttempt();
        }
    }

    public void execute(Game game, SolutionList solutionList) {

        Solution solution = new Solution();
        solution.setGame(game);
        currentSolutionGame = solution;
        currentSolutionGameList = solutionList;
        //gameStrategy.setGame(solution.getGame());

        // This first game make new tries....
        execute(solution);

       /* gameStrategy.setGame(currentSolutionGame.getGame());
        if (gameStrategy.endOfGame() && !gameStrategy.errorInGame())
            solutionList.add(currentSolutionGame.clone());

        // Execute other retries....
        while (solutionList.size() <= getMaxSolution() && !backupGameStack.empty()) {

            restoreLastAttempt();
            gameStrategy.setGame(currentSolutionGame.getGame());

            // Tries to solve game (and generate others games possibilites)
            execute();

            gameStrategy.setGame(currentSolutionGame.getGame());

            // If is correct ... Add to list solution... else... restore of list
            boolean endOfGame = gameStrategy.endOfGame();
            boolean errorInGame = gameStrategy.errorInGame();
            if (endOfGame && !errorInGame) {
                currentSolutionGameList.add(currentSolutionGame); // Good game ... it's to add out list.
            } else {
                logger.debug("Restore Game: EndOfGame = "  + endOfGame + " ErrorInGame = " + errorInGame + "\n" + currentSolutionGame.getGame());
                restoreLastAttempt();
            }
        }*/
    }


    private boolean makePathOfGame() {

        Solution solution = currentSolutionGame;
        Game game = solution.getGame();

        // Find first empty cells and make one scene.
        for (int row = 0; row < Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME; row++) {

            for (int col = 0; col < Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME; col++) {

                Cell cell = game.getCol(col).get(row);
                ArrayList<Integer> possibilities = cell.getPossibitiesInt();

                if (!cell.hasValue() && !cell.isFixedValue() && possibilities.size() > 1) {

                    // Make picture of scene for restore one position....
                    for (Integer val : possibilities) {
                        BoxResult boxResult = new BoxResult(new Position(row, col), val);
                        // Make thr possible solution, (Solution, Value and Position)
                        Solution solutionClonable = solution.clone();
                        BackupGame backupGameItem = new BackupGame(solutionClonable, boxResult);
                        backupGameStack.push(backupGameItem);
                    }
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public void setGameStrategy(GameStrategyAbstract gameStrategy) {
        this.gameStrategy = gameStrategy;
    }

    @Override
    public void setStepInterface(StepExecute stepExecuteInterface) {
        this.stepExecuteInterface = stepExecuteInterface;
    }

    @Override
    public int getMaxSolution() {
        return maxSolutions;
    }

    @Override
    public Solution getSolution() {
        return currentSolutionGame;
    }

    @Override
    public SolutionList getSolutions() {
        return currentSolutionGameList;
    }

    @Override
    public void setMaxSolutions(int maxSolutions) {
        this.maxSolutions = maxSolutions;
    }
}
