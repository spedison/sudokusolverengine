/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sudoku.kernel.solvers;

import br.com.sudoku.kernel.interfaces.Consts;
import br.com.sudoku.kernel.interfaces.GameStrategyAbstract;
import br.com.sudoku.kernel.interfaces.PerformGameInterface;
import br.com.sudoku.kernel.interfaces.StepExecute;
import br.com.sudoku.kernel.vo.Cell;
import br.com.sudoku.kernel.vo.Game;
import br.com.sudoku.kernel.vo.results.BoxResult;
import br.com.sudoku.kernel.vo.results.Solution;
import br.com.sudoku.kernel.vo.results.SolutionList;
import br.com.sudoku.kernel.vo.results.Step;

import java.util.Random;

/**
 * @author @GrupoAlpha
 */
public class PerformOneGameKick implements PerformGameInterface {

    private Random rnd;
    private boolean usedKick;
    private StepExecute stepExecuteInterface;
    private GameStrategyAbstract gameStrategy;
    private boolean stepOk;
    private Solution currentSolution;

    public PerformOneGameKick() {
        rnd = new Random(System.currentTimeMillis());
        stepExecuteInterface = null;
    }


    public boolean verifyBasicCondition(GameStrategyAbstract game, BoxResult boxResult) {
        return game.canApplyResultBox(boxResult)
                && game.canApplyResultCol(boxResult)
                && game.canApplyResultRow(boxResult);
    }

    public void execute(Game gameVO, Solution solution) {
        solution.setGame(gameVO);
        execute(solution);
    }

    @Override
    public void execute(Game game, SolutionList solutionList) {
        Solution solution = new Solution();
        execute(game, solution);
        solutionList.add(solution);
    }


    private boolean kickValue(Game game, BoxResult boxResult, int tryNumber) {

        int count = 0;

        // Find empty cells.
        for (int row = 0; row < Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME; row++) {
            for (int col = 0; col < Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME; col++) {
                Cell cell = game.getCol(col).get(row);
                Cell.ReturnCountPossibilities returnCountPossibilities = cell.countPossibilities();
                if (!cell.hasValue() && returnCountPossibilities.getCount() >= 2) {
                    if ((count++) == tryNumber) {

                        boxResult.getPos().setCol(col);
                        boxResult.getPos().setRow(row);

                        int rndPos = rnd.nextInt(returnCountPossibilities.getCount());
                        boxResult.setValue(cell.hasMinPossibilities(rndPos));
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public void setGameStrategy(GameStrategyAbstract gameStrategy) {
        this.gameStrategy = gameStrategy;
    }

    @Override
    public void setStepInterface(StepExecute stepExecuteInterface) {
        this.stepExecuteInterface = stepExecuteInterface;
    }


    @Override
    public void execute(Solution solution) {

        Game gameVO = solution.getGame();
        int numberOfIteration = 0;
        int numberOfRetry = 0;
        BoxResult boxResult = new BoxResult();

        boolean doBakup = false;
        Game bkpGame = new Game();
        int bkpCounter = 0;

        int lastExecute = -1;
        int tryNumber = 0;
        usedKick = false;

        currentSolution = solution;

        solution.setGame(gameVO);
        GameStrategyAbstract game = gameStrategy;
        game.setGame(gameVO);


        do {
            boolean canExecute = false;
            boxResult.reset();

            canExecute = game.getUniqueValueRow(boxResult);
            if (canExecute)
                canExecute &= verifyBasicCondition(game, boxResult);

            if (canExecute) {
                lastExecute = 0;
            }

            if (!canExecute) {
                canExecute = game.getUniqueValueCol(boxResult);
                if (canExecute)
                    canExecute &= verifyBasicCondition(game, boxResult);

                if (canExecute) {
                    lastExecute = 2;
                }
            }

            if (!canExecute) {
                canExecute = game.getUniqueValueBox(boxResult);
                if (canExecute)
                    canExecute &= verifyBasicCondition(game, boxResult);

                if (canExecute) {
                    lastExecute = 1;
                }
            }

            if (!canExecute) {
                canExecute = game.findUniqueValue(boxResult);
                if (canExecute)
                    canExecute &= verifyBasicCondition(game, boxResult);

                if (canExecute) {
                    lastExecute = 3;
                }
            }

            // MUDAR AQUI PARA GANHAR RECURSIVIDADE !!!! E ter mais soluções.....
            if (!canExecute && numberOfRetry >= 1) {

                boolean retKick = kickValue(gameVO, boxResult, tryNumber);

                tryNumber++;

                canExecute = retKick && verifyBasicCondition(game, boxResult);


                if (canExecute) {
                    usedKick = true;
                }

                if (!retKick) {

                    // Retore backup
                    if (doBakup) {
                        tryNumber = 0;
                        numberOfIteration = bkpCounter;
                        gameVO.copy(bkpGame);
                        doBakup = false;
                        canExecute = false;
                    }

                }

                lastExecute = 4;
            }

            if (canExecute) { // Yes can apply changes.

                // If I do Kick ... then I will backup data.
                if (lastExecute == 4 && !doBakup) {
                    bkpCounter = numberOfIteration;
                    bkpGame.copy(gameVO);
                    doBakup = true;
                }

                // Write the solution.
                game.writeResult(boxResult, solution.getStepList());

                // If have error in game, restore data.
                if (game.errorInGame()) {
                    doBakup = false;
                    numberOfIteration = bkpCounter;
                    gameVO.copy(bkpGame);
                }

                if (stepExecuteInterface != null)
                    stepExecuteInterface.processedOneStep(new Step(0, boxResult.getPos().clone(), boxResult.getValue()), gameVO);


                lastExecute = -1;
                tryNumber = 0;
                numberOfIteration++;
            }


            if (!canExecute) {
                numberOfRetry++;
            } else {
                numberOfRetry = 0;
            }
        } while (!game.errorInGame() && !game.endOfGame());

    }

    @Override
    public int getMaxSolution() {
        return 1;
    }

    @Override
    public Solution getSolution() {
        return currentSolution;
    }

    @Override
    public SolutionList getSolutions() {
        return null;
    }
}
