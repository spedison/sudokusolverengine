package br.com.sudoku.kernel.solvers;

import br.com.sudoku.kernel.interfaces.Consts;
import br.com.sudoku.kernel.interfaces.GameStrategyAbstract;
import br.com.sudoku.kernel.vo.BoxCell;
import br.com.sudoku.kernel.vo.Cell;
import br.com.sudoku.kernel.vo.Game;
import br.com.sudoku.kernel.vo.groupelements.GroupBase;
import br.com.sudoku.kernel.vo.results.BoxResult;
import org.apache.log4j.Logger;


/**
 * Created by @GrupoAlpha on 25/05/15.
 */
public class GameStrategy extends GameStrategyAbstract {


    private static Logger logger = Logger.getLogger(GameStrategy.class);

    public GameStrategy() {
        super();
    }

    public GameStrategy(Game game) {
        super(game);
    }

    @Override
    public boolean getUniqueValueRow(BoxResult boxResult) {
        for (GroupBase r : game.getCellRows()) {
            if (r.uniqueValue(boxResult))
                return true;
        }
        return false;
    }

    @Override
    public boolean getUniqueValueCol(BoxResult boxResult) {
        for (GroupBase c : game.getCellCols()) {
            if (c.uniqueValue(boxResult))
                return true;
        }
        return false;
    }

    @Override
    public boolean getUniqueValueBox(BoxResult boxResult) {
        for (int row = 0; row < Consts.SIZE_OF_GAME; row++) {
            for (int col = 0; col < Consts.SIZE_OF_GAME; col++) {
                if (game.getBox(row, col).uniqueValueInBox(boxResult)) {
                    // Caution: This point the result is not ready to use.... Ajust position is needed.
                    boxResult.getPos().setCol(col * Consts.SIZE_OF_BOX + boxResult.getPos().getCol());
                    boxResult.getPos().setRow(row * Consts.SIZE_OF_BOX + boxResult.getPos().getRow());
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean findUniqueValue(BoxResult boxResult) {
        int col;
        for (GroupBase row : game.getCellRows()) {

            col = 0;

            for (Cell c : row) {
                Cell.ReturnCountPossibilities result = c.countPossibilities();
                int count = result.getCount();
                // If have one possibility. Return.
                if (count == 1) {
                    boxResult.setValue(result.getValue());
                    boxResult.getPos().setCol(col);
                    boxResult.getPos().setRow(row.getNumber());
                    boxResult.setResult(true);
                    logger.debug("FindUnique is true for:" + boxResult);
                    break;
                }

                col++;

            }
        }

        return false;
    }

    @Override
    public boolean canApplyResultRow(BoxResult boxResult) {

        if (!boxResult.isResult()) {
            logger.debug("BoxResult is false:" + boxResult);
            return false;
        }

        for (Cell c : game.getCellRows().get(boxResult.getPos().getRow())) {
            if (boxResult.getValue() == c.getValue()) {
                String rowString = game.getCellRows().get(boxResult.getPos().getRow()).toString();
                logger.debug("Can not apply value " + boxResult + "in row" + rowString);
                return false;
            }
        }

        logger.debug("Can write value in Row:" + boxResult);
        return true;
    }

    @Override
    public boolean canApplyResultCol(BoxResult boxResult) {

        if (!boxResult.isResult())
            return false;

        for (Cell c : game.getCellCols().get(boxResult.getPos().getCol())) {
            if (boxResult.getValue() == c.getValue())
                return false;
        }

        return true;
    }

    @Override
    public boolean canApplyResultBox(BoxResult boxResult) {

        BoxCell boxCell = game.getBoxPositionGame(boxResult.getPos());

        for (int row = 0; row < Consts.SIZE_OF_BOX; row++) {
            for (int col = 0; col < Consts.SIZE_OF_BOX; col++) {
                if (boxCell.getCell(row, col).getValue() == boxResult.getValue())
                    return false;
            }
        }
        return true;
    }

    @Override
    public boolean errorInGame() {
        for (GroupBase r : game.getCellRows()) {
            for (Cell c : r) {
                if ((!c.hasValue()) && c.isNoPossibility())
                    return true;
            }
        }
        return false;
    }

    @Override
    public boolean endOfGame() {
        for (GroupBase r : game.getCellRows()) {
            for (Cell c : r) {
                if (!(c.hasValue() && c.isNoPossibility()))
                    return false;
            }
        }
        return true;
    }

}
