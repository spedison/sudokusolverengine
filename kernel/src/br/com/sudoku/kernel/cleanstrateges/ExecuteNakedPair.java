package br.com.sudoku.kernel.cleanstrateges;

import br.com.sudoku.kernel.solvers.GameStrategy;
import br.com.sudoku.kernel.vo.Cell;
import br.com.sudoku.kernel.vo.Game;
import br.com.sudoku.kernel.vo.groupelements.GroupBase;
import br.com.sudoku.utils.ArrayUtil;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * File ExecuteNakedPair.java created by @GrupoAlpha on 31/07/2015 at 05:23 for projetct SudokuSolverEngine.
 */
public class ExecuteNakedPair {

    Game game;
    GameStrategy gameStrategy;

    static private Logger logger = Logger.getLogger(ExecuteNakedPair.class);

    public ExecuteNakedPair() {
        game = null;
        gameStrategy = new GameStrategy();
    }

    public void setGame(Game game) {
        this.game = game;
        gameStrategy.setGame(this.game);
    }

    public void reset() {
        game = null;
        gameStrategy.setGame(null);
    }

    public void processGame(Game game) {
        setGame(game);
        processFindPairCell();
    }

    private void processFindPairCell() {

        assert game != null;

        for (GroupBase col : game.getCellCols()) {
            while (processFindPairCell(col)) ;
        }

        for (GroupBase row : game.getCellRows()) {
            while (processFindPairCell(row)) ;
        }
        // Depois que funcionar colocar arqui uma forma simples de acessar e limpar os grupos.

    }

    /***
     * Find one pair with 2 itens equals. The row or col is cleaned after found itens.
     *
     * @param cellGroup Row, Col or single group.
     * @return true if process one item. If not found item return false.
     */
    private boolean processFindPairCell(GroupBase cellGroup) {

        boolean returned = false;

        // Permute and tests found pairs. (I will procecess one pair)
        for (int p1 = 0; p1 < cellGroup.size() - 1; p1++) {
            for (int p2 = p1 + 1; p2 < cellGroup.size(); p2++) {

                Cell c1 = cellGroup.get(p1);
                Cell c2 = cellGroup.get(p2);


                if (c1.hasValue() || c2.hasValue())
                    continue;

                if (c1.isFixedValue() || c2.isFixedValue())
                    continue;

                if (c1.countPossibilities().getCount() != 2 || c2.countPossibilities().getCount() != 2)
                    continue;

                boolean[] result = ArrayUtil.and(
                        c1.getPossibilitiesValues(),
                        c2.getPossibilitiesValues());

                if (ArrayUtil.countTrue(result) == 2) {
                    if (p1 != p2) {
                        logger.debug("Pair found in : " + cellGroup + " P1=" + p1 + " P2=" + p2);
                        if (cleanGroup(cellGroup, p1, p2))
                            returned |= true;
                    }
                }
            }
        }

        return returned;
    }

    private boolean cleanGroup(GroupBase cellGroup, int pos1, int pos2) {

        int countProcessed = 0;

        // If not found equal pair, return false
        if (pos1 == -1 || pos2 == -1)
            return false;

        List<Integer> itensToRemove = cellGroup.get(pos1).getPossibitiesInt();

        if (itensToRemove.size() < 2) {
            logger.error("itensToRemove is less then 2 itens" + cellGroup);
            return false;
        }

        int val1 = itensToRemove.get(0);
        int val2 = itensToRemove.get(1);

        // Clean line or col, if found one pair.
        for (int pos = 0; pos < cellGroup.size(); pos++) {

            Cell c = cellGroup.get(pos);

            if (c.isFixedValue())
                continue;

            if (c.hasValue())
                continue;

            if (pos == pos1 || pos == pos2)
                continue;

            countProcessed += c.removePossibity(val1) ? 1 : 0;
            countProcessed += c.removePossibity(val2) ? 1 : 0;
        }

        return countProcessed > 1;
    }
}

