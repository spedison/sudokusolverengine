package br.com.sudoku.kernel.loadersgame;

import br.com.sudoku.kernel.interfaces.Consts;
import br.com.sudoku.kernel.vo.Game;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.Random;

/**
 * File LoaderGameFileTextResolved.java created by @GrupoAlpha on 29/05/2015 at 19:03 for projetct SudokuSolverEngine.
 */
public class LoaderGameFileTextResolved extends LoaderGameBasic {


    private static Logger logger = Logger.getLogger(LoaderGameFileTextResolved.class);

    public boolean lineToGame(String strGame, Game game) {

        if (!strGame.startsWith((Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME) + "," + (Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME) + "-"))
            return false;

        StringBuffer buffer = new StringBuffer(strGame.substring(4));

        int row = 0;
        int col = 0;
        for (int x = 0; x < buffer.length(); x++) {

            char ch = buffer.charAt(x);

            if (ch == '.')
                continue;

            if(ch != '*')
                setData(row, col, (ch - '0'));

            col++;

            if (col == (Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME)) {
                col = 0;
                row++;
            }
        }

        game.copy(this.game);
        return true;
    }

    @Override
    public boolean load(Game game) {

        //
        this.game = game;

        // Convert Line to GAME
        return lineToGame(loadLine(-1), game);

    }

    private String loadLine(int pos) {

        File file = new File(br.com.sudoku.utils.ConfigEnviroment.getHomeDir() + "/git/sudokusolverengine/files/seeds.txt");
        ArrayList<String> lines = new ArrayList<>();

        try {
            // Load lines of file game cache.
            loadFileLines(file, lines);

            if (pos == -1) {
                // Choose one line to convert in game.
                Random rnd = new Random(System.currentTimeMillis());
                return lines.get(rnd.nextInt(lines.size() - 1));
            }

            if (pos >= lines.size())
                logger.error("Get invalid item: " + pos + " - size: " + lines.size());

            return lines.get(pos);

        } catch (FileNotFoundException e) {
            return "";

        } catch (IOException e) {
            return "";
        }
    }

    public boolean load(Game game, int posLine) {

        this.game = game;

        // Convert Line to GAME
        return lineToGame(loadLine(posLine), game);


    }

    private void loadFileLines(File arquivo, ArrayList<String> lines) throws IOException {

        // Read lines of game.
        FileReader fileReader = new FileReader(arquivo);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line;

        while ((line = bufferedReader.readLine()) != null) {
            if (line != null && !line.isEmpty() && !line.startsWith("#"))
                lines.add(line);
        }

        // Close the file..
        bufferedReader.close();
        fileReader.close();
    }
}
