/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sudoku.kernel.loadersgame;

import br.com.sudoku.kernel.interfaces.Consts;
import br.com.sudoku.kernel.solvers.GameStrategy;
import br.com.sudoku.kernel.vo.Game;
import br.com.sudoku.kernel.vo.Position;

import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author @GrupoAlpha
 */
public class LoaderFileTextToSolve extends LoaderGameBasic {

    private LoaderGameFileTextResolved loader;
    private Random rand;

    private Game filled;
    private int pointsDefined;
    private GameStrategy gm;

    static final public String POINTS_DEFINED = "POINTS_DEFINED";

    public LoaderFileTextToSolve() {

        gm = new GameStrategy();
        filled = new Game();
        loader = new LoaderGameFileTextResolved();
        rand = new Random(System.currentTimeMillis());
    }

    @Override
    public void setProperty(String name, int value) {
        if (name.equalsIgnoreCase(POINTS_DEFINED)) {
            pointsDefined = value;
        }
    }

    @Override
    public boolean load(Game game) {

        Set<Position> positionList = new LinkedHashSet<>();
        loader.load(filled);

        for (int x = 0; x < pointsDefined; x++) {

            Position pos = new Position(
                    rand.nextInt(Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME),
                    rand.nextInt(Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME));

            if (!positionList.add(pos))
                x--;

        }

        gm.setGame(game);
        gm.copy(filled, positionList.stream().collect(Collectors.toList()));

        return true;
    }
}


/*
    void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void setProperties(Properties value) {
        if (value.containsKey("FILE")) {
            setFileName(value.getProperty("FILE"));
        } else {
            if (value.containsKey("FILENAME")) {
                setFileName(value.getProperty("FILENAME"));
            }
        }
    }

    @Override
    public boolean load(Game game) {

        // Format of file
        // using the line ::>> [row1],[column1],[value1]
        //                     [row2],[column2],[value2]

        this.game = game;

        FileInputStream in = null;
        try {
            in = new FileInputStream(fileName);
            DataInputStream dis = new DataInputStream(in);
            BufferedReader br = new BufferedReader(new InputStreamReader(dis));
            String line;
            while ((line = br.readLine()) != null) {
                String[] strings = line.trim().split(",");
                if (strings.length == 3) {
                    int row, col, value;
                    row = Integer.parseInt(strings[0].trim());
                    col = Integer.parseInt(strings[1].trim());
                    value = Integer.parseInt(strings[2].trim());
                    setData(row, col, value);
                }
            }
            br.close();
        } catch (IOException ex) {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ex1) {
                    Logger.getLogger(LoaderFileTextToSolve.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
            ex.printStackTrace();
            return false;
        }

        return true;
    }
    private String fileName;*/

