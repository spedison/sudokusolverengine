/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sudoku.kernel.loadersgame;

import br.com.sudoku.kernel.vo.Game;

/**
 *
 * @author @GrupoAlpha
 */
public class LoaderGameMediumImpossible extends LoaderGameBasic {

    @Override
    public boolean load(Game game) {
        this.game = game;
        setData(2, 5, 8);
        setData(3, 4, 9);
        setData(4, 0, 9);
        setData(5, 6, 1);
        setData(6, 2, 3);
        setData(6, 7, 4);
        setData(7, 3, 3);

        return true;
    }
}
