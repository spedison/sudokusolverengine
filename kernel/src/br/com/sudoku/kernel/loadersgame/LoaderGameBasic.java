/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sudoku.kernel.loadersgame;

import br.com.sudoku.kernel.interfaces.LoaderGameInterface;
import br.com.sudoku.kernel.vo.Game;

/**
 * @author @GrupoAlpha
 */
public abstract class LoaderGameBasic implements LoaderGameInterface {

    protected Game game;

    public void setData(int row, int col, int value) {

        game.getRow(row).get(col).setFixedValue(true);
        game.getRow(row).get(col).setValue(value - 1);
        game.getCol(col).removePossibility(value - 1);
        game.getRow(row).removePossibility(value - 1);
        game.getBoxPositionGame(row, col).removePossibility(value - 1);
    }

}
