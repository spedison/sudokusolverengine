/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sudoku.kernel.loadersgame;

import br.com.sudoku.kernel.vo.Game;

/**
 *
 * @author @GrupoAlpha
 */
public class LoaderGameMediumHard extends LoaderGameBasic {

    @Override
    public boolean load(Game game) {
        this.game = game;
        setData(0, 3, 2);
        //setData(0, 6, 7);

        setData(1, 0, 5);
        setData(1, 3, 1);
        setData(1, 7, 9);
        setData(1, 8, 2);

        setData(2, 0, 6);
        setData(2, 1, 9);
        //setData(2, 3, 7);
        setData(2, 5, 8);
        setData(2, 7, 1);

        setData(3, 1, 3);
        //setData(3, 2, 6);
        setData(3, 4, 9);
        setData(3, 7, 7);
        setData(3, 8, 5);

        setData(4, 0, 9);
        setData(4, 3, 4);
        setData(4, 4, 7);
        //setData(4, 5, 6);

        setData(5, 1, 4);
        setData(5, 3, 5);
        setData(5, 4, 2);
        setData(5, 6, 1);

        setData(6, 2, 3);
        setData(6, 4, 8);
        setData(6, 6, 5);
        //setData(6, 7, 4);

        setData(7, 1, 6);
        setData(7, 3, 3);
        setData(7, 4, 4);
        setData(7, 5, 2);
        setData(7, 7, 8);
        //SetData(8, 2, 5);
        //SetData(8, 4, 4);
        //SetData(1, col,);
        return true;
    }
}
