package br.com.sudoku.kernel.loadersgame;

import br.com.sudoku.kernel.interfaces.LevelGameType;
import br.com.sudoku.kernel.makersgame.MakerGame;
import br.com.sudoku.kernel.solvers.GameStrategy;
import br.com.sudoku.kernel.vo.Game;

import java.util.Random;

/**
 * File LoaderRandonGame.java created by @GrupoAlpha on 27/05/2015 at 19:20 for projetct SudokuSolverEngine.
 */
public class LoaderRandonGame extends LoaderGameBasic {

    @Override
    public boolean load(Game game) {

        MakerGame mg = new MakerGame();
        GameStrategy gs = new GameStrategy();
        Random rnd = new Random(System.currentTimeMillis());
        LevelGameType [] possibitiesTypes = LevelGameType.values();
        LevelGameType t = possibitiesTypes[rnd.nextInt(possibitiesTypes.length)];
        game.copy(mg.makeGame(gs, t));
        return true;
    }
}
