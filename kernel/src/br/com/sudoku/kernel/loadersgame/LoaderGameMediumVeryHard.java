/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sudoku.kernel.loadersgame;

import br.com.sudoku.kernel.vo.Game;

/**
 *
 * @author @GrupoAlpha
 */
public class LoaderGameMediumVeryHard extends LoaderGameBasic {

    @Override
    public boolean load(Game game) {
        this.game = game;

        //SetData(1, 0, 5);
        //SetData(1, 7, 9);
        //SetData(1, 8, 2);

        //SetData(2, 0, 6);
        //SetData(2, 3, 7);
        setData(2, 5, 8);

        setData(3, 4, 9);
        setData(3, 7, 7);
        setData(3, 8, 5);

        setData(4, 0, 9);
        //SetData(4, 3, 4);
        setData(4, 5, 6);

        //SetData(5, 1, 4);
        setData(5, 6, 1);

        setData(6, 2, 3);
        setData(6, 7, 4);

        setData(7, 3, 3);
        //SetData(7, 5, 2);

        setData(6, 1, 9);
        return true;
    }
}
