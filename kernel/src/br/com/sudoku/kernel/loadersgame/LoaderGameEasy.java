/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sudoku.kernel.loadersgame;

import br.com.sudoku.kernel.vo.Game;

/**
 *
 * @author @GrupoAlpha
 */
public class LoaderGameEasy extends LoaderGameBasic {

    public LoaderGameEasy() {
    }

    @Override
    public boolean load(Game game) {

        this.game = game;

        setData(0, 2, 2);
        setData(0, 5, 7);
        setData(0, 7, 8);

        setData(1, 4, 1);
        setData(1, 6, 4);
        setData(1, 8, 3);

        setData(2, 0, 4);
        setData(2, 1, 3);
        setData(2, 7, 6);
        setData(2, 8, 2);

        setData(3, 0, 8);
        setData(3, 1, 5);
        setData(3, 4, 4);
        setData(3, 6, 9);

        setData(4, 1, 2);
        setData(4, 2, 4);
        setData(4, 3, 5);
        setData(4, 5, 3);
        setData(4, 6, 8);
        setData(4, 7, 1);

        setData(5, 2, 9);
        setData(5, 4, 8);
        setData(5, 7, 4);
        setData(5, 8, 5);

        setData(6, 0, 7);
        setData(6, 1, 1);
        setData(6, 7, 9);
        setData(6, 8, 4);

        setData(7, 0, 2);
        setData(7, 2, 3);
        setData(7, 4, 6);

        setData(8, 1, 4);
        setData(8, 3, 9);
        setData(8, 6, 3);

        return true;

    }
}
