/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sudoku.kernel.loadersgame;


import br.com.sudoku.kernel.vo.Game;

/**
 *
 * @author @GrupoAlpha
 */
public class LoaderGameMedium extends LoaderGameBasic {

    @Override
    public boolean load(Game game) {
        this.game = game;
        setData(0, 1, 3);
        setData(0, 3, 2);
        setData(0, 6, 5);
        setData(0, 8, 7);

        setData(1, 0, 5);
        setData(1, 3, 6);
        setData(1, 5, 9);

        setData(2, 0, 4);
        setData(2, 2, 1);
        setData(2, 4, 7);
        setData(2, 6, 2);

        setData(3, 0, 7);
        setData(3, 1, 9);
        setData(3, 5, 5);
        setData(3, 7, 3);

        setData(4, 2, 2);
        setData(4, 4, 9);
        setData(4, 7, 7);

        setData(5, 0, 1);
        setData(5, 2, 6);
        setData(5, 3, 3);
        setData(5, 5, 7);

        setData(6, 2, 9);
        setData(6, 3, 7);
        setData(6, 5, 2);
        setData(6, 6, 1);
        setData(6, 7, 5);
        setData(6, 8, 8);

        setData(7, 0, 8);
        setData(7, 4, 5);
        setData(7, 7, 2);
        setData(7, 8, 4);

        setData(8, 0, 2);
        setData(8, 2, 5);
        setData(8, 4, 4);
        //SetData(1, col,);
        return true;
    }
}
