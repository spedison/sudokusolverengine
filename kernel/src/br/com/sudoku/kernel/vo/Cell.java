/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sudoku.kernel.vo;

import br.com.sudoku.kernel.interfaces.Consts;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * This class refer to Game's basic unit  (one unique cell)
 *
 * @author @GrupoAlpha
 */
public class Cell implements Serializable, Cloneable {

    private int value;
    private boolean possibilitiesValues[];
    private int possibitiesCount;
    private boolean fixedValue;
    private Position pos;

    public Cell() {
        possibilitiesValues = new boolean[Consts.NUMBER_OF_SIMBOLS];
        value = Consts.INVALID_VALUE;
        fixedValue = false;
        addAllPossibities();
    }

    public void addAllPossibities() {
        for (int x = 0; x < possibilitiesValues.length; x++) {
            possibilitiesValues[x] = true;
        }
        possibitiesCount = 9;
    }

    public void removeAllPossibilities() {
        for (int x = 0; x < possibilitiesValues.length; x++) {
            possibilitiesValues[x] = false;
        }
        possibitiesCount = 0;
    }

    public boolean removePossibity(int pos) {

        if (possibilitiesValues[pos] == false
                || possibitiesCount == 0)
            return false;

        possibilitiesValues[pos] = false;
        possibitiesCount--;
        return true;
    }

    public boolean checkPossibity(int pos) {

        if (isFixedValue() || hasValue()) {
            return false;
        }

        return possibilitiesValues[pos] == true;
    }

    public boolean hasValue() {
        return value != Consts.INVALID_VALUE;
    }

    public void setValue(int pos) {
        assert (pos >= 0 && pos <= Consts.NUMBER_OF_SIMBOLS);
        value = pos;
        removeAllPossibilities();
    }

    public int getValue() {
        return value;
    }

    public int hasMinPossibilities(int number) {

        if (hasValue() || isFixedValue())
            return 0;

        int count = -1;
        for (int x = 0; x < possibilitiesValues.length; x++) {
            if (checkPossibity(x)) {
                count++;

                if (number == count) {
                    return x;
                }
            }
        }
        return -1;
    }

    public boolean isNoPossibility() {
        for (boolean i : getPossibilitiesValues()) {
            if (i)
                return false;
        }
        return true;
    }

    public class ReturnCountPossibilities {

        private int count;
        private int value;

        public ReturnCountPossibilities() {
            value = Consts.INVALID_VALUE;
            count = 0;
        }

        public int getCount() {
            return count;
        }

        public int getValue() {
            return value;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public void incCount() {
            count++;
        }
    }

    public ReturnCountPossibilities countPossibilities() {

        ReturnCountPossibilities returnCountPossibilities = new ReturnCountPossibilities();

        if (isFixedValue() || hasValue() || possibitiesCount == 0) {
            return returnCountPossibilities;
        }

        if (possibitiesCount > 1) {
            returnCountPossibilities.setCount(possibitiesCount);
            returnCountPossibilities.setValue(1);
            return returnCountPossibilities;
        }


        for (int x = 0; x < possibilitiesValues.length; x++) {

            if (possibilitiesValues[x] == true) {
                returnCountPossibilities.incCount();
                returnCountPossibilities.setValue(x);
                return returnCountPossibilities;
            }
        }

        return returnCountPossibilities;
    }

    public ArrayList<Integer> getPossibitiesInt() {
        int c = 0;
        ArrayList<Integer> ret = new ArrayList<>();

        for (boolean b : getPossibilitiesValues()) {

            if (b)
                ret.add(c);

            c++;
        }
        return ret;
    }

    public String getPossibitiesString() {

        int c = 0;
        StringBuffer out = new StringBuffer();

        for (boolean b : getPossibilitiesValues()) {

            if (b)
                out.append(Consts.SIMBOLS[c]);

            c++;
        }

        return out.toString();
    }

    /**
     * @return the fixedValue
     */
    public boolean isFixedValue() {
        return fixedValue;
    }

    /**
     * @param fixedValue the fixedValue to set
     */
    public void setFixedValue(boolean fixedValue) {
        this.fixedValue = fixedValue;
    }

    public boolean[] getPossibilitiesValues() {
        return possibilitiesValues;
    }

    //public void setPossibilitiesValues(boolean[] possibilitiesValues) {
    //    this.possibilitiesValues = possibilitiesValues;
    //}

    /**
     * @return the pos
     */
    public Position getPos() {
        return pos;
    }

    /**
     * @param pos the pos to set
     */
    public void setPos(Position pos) {
        this.pos = pos.clone();
    }

    public Cell clone() {
        Cell ret = new Cell();
        ret.copy(this);
        return ret;
    }

    public void copy(final Cell ret) {
        value = ret.value;
        fixedValue = ret.fixedValue;
        value = ret.value;
        System.arraycopy(ret.possibilitiesValues, 0, possibilitiesValues, 0, possibilitiesValues.length);
        possibitiesCount = ret.possibitiesCount;
    }

    @Override
    public String toString() {

        String out = "";

        if (isFixedValue())
            out += "[";
        else
            out += " ";

        out += pos.toString();
        out += " = ";

        if (hasValue()) {
            out += Consts.SIMBOLS[value];
        } else {
            out += "*";
        }

        if (isFixedValue())
            out += "]";
        else
            out += " ";

        return out;
    }
}
