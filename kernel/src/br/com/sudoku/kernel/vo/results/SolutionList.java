package br.com.sudoku.kernel.vo.results;

import java.util.LinkedList;

/**
 * File SolutionList.java created by @GrupoAlpha on 31/05/2015 at 22:56 for projetct SudokuSolverEngine.
 */
public class SolutionList extends LinkedList<Solution> {

    @Override
    public boolean add(Solution solution) {

        System.out.println("\n\n>>>>>>>------  JOGO ADICIONADO !!!  -----<<<<<<< \n\n");

        for (Solution item : this) {
            if (item.getGame().equals(solution.getGame()))
                return false;
        }

        return super.add(solution);
    }
}
