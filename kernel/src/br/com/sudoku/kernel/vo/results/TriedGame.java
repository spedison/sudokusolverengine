package br.com.sudoku.kernel.vo.results;

import br.com.sudoku.kernel.vo.Game;

/**
 * Created by @GrupoAlpha on 24/05/15.
 */
public class TriedGame {

    private Game game;
    private BoxResult result;
    private int numberTried;

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public BoxResult getResult() {
        return result;
    }

    public void setResult(BoxResult result) {
        this.result = result;
    }

    public int getNumberTried() {
        return numberTried;
    }

    public void setNumberTried(int numberTried) {
        this.numberTried = numberTried;
    }
}
