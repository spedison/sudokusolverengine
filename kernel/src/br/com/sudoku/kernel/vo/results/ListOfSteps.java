package br.com.sudoku.kernel.vo.results;

import br.com.sudoku.kernel.exceptions.ErrorEndOfGame;
import br.com.sudoku.kernel.exceptions.ErrorStepNoFound;
import br.com.sudoku.kernel.exceptions.ErrorValueNotChanged;
import br.com.sudoku.kernel.interfaces.Consts;
import br.com.sudoku.kernel.solvers.GameStrategy;
import br.com.sudoku.kernel.vo.Cell;
import br.com.sudoku.kernel.vo.Position;

import java.util.ArrayList;

/**
 * File ListOfSteps.java created by @GrupoAlpha on 11/06/2015 at 08:50 for projetct SudokuSolverEngine.
 */
public class ListOfSteps extends ArrayList<Step> {

    int stepUser;

    public ListOfSteps() {
        super();
        stepUser = 0;
    }


    /***
     * Using one game the StepList is filled with all steps for fill.
     * @param gameStrategy - The Game
     * @throws ErrorEndOfGame - If game is all filled. End Of Game.
     */
    public void addAllSteps(GameStrategy gameStrategy) throws ErrorEndOfGame {

        if (gameStrategy.endOfGame())
            throw new ErrorEndOfGame();

        for (int row = 0; row < Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME; row++) {

            for (int col = 0; col < Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME; col++) {

                Cell cell = gameStrategy.getGame().getCol(col).get(row);

                if (!(cell.hasValue() || cell.isFixedValue())) {
                    add(new Step(0, new Position(row, col), Consts.INVALID_VALUE));
                }

            }
        }
    }

    /**
     * Define value of one item item. Check if do not found control or control do not change.
     * @param row -  Row item
     * @param col - Column item
     * @param value - value item
     * @throws ErrorValueNotChanged - If item do not change value.
     * @throws ErrorStepNoFound - If item do not exists, because control do not exists.
     */
    public void addOneStep(int row, int col, int value) throws ErrorValueNotChanged, ErrorStepNoFound {

        Step tmpIndex = new Step(0, new Position(row, col), value);
        int item = indexOf(tmpIndex);

        if (item == -1)
            throw new ErrorStepNoFound();

        Step st = get(item);
        if (st.equals(tmpIndex) && st.getValue() == value)
            throw new ErrorValueNotChanged();

        st.setValue(value);
        st.setStep(stepUser++);
    }

    /***
     * Get the first position Cell than it is empty
     * @return - Position empty cell
     * @throws ErrorEndOfGame - If game is all filled, so it is game over.
     */
    public Position getFirstEmpty() throws ErrorEndOfGame {

        for (Step item : this) {
            if (item.getValue() == Consts.INVALID_VALUE) {
                return item.getPosition();
            }
        }

        throw new ErrorEndOfGame();
    }

    /***
     * Get all Empty cells
     * @return - Numbers of empty cells.
     */
    public int getCountEmptyFields() {
        return (int) stream().
                filter(i -> i.getValue() == Consts.INVALID_VALUE).
                count();
    }
}
