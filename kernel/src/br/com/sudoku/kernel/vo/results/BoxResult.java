/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sudoku.kernel.vo.results;

import br.com.sudoku.kernel.interfaces.Consts;
import br.com.sudoku.kernel.vo.Position;

import java.io.Serializable;

/**
 * @author @GrupoAlpha
 */
public class BoxResult implements Serializable {

    private Position pos;
    private int value;
    private boolean result;

    public BoxResult() {
        pos = new Position();
        reset();
    }

    public BoxResult(Position pos, int value, boolean result) {
        this.pos = pos;
        this.value = value;
        this.result = result;
    }

    public BoxResult(Position pos, int value) {
        this.pos = pos.clone();
        this.value = value;
        this.result = true;
    }

    public Position getPos() {
        return pos;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public void reset() {
        setResult(false);
        setValue(Consts.INVALID_VALUE);
        getPos().setCol(-1);
        getPos().setRow(-1);
    }

    @Override
    public String toString() {
        return "BoxResult:(" + pos + "=" + value + ";R:" + result + ")" ;
    }
}
