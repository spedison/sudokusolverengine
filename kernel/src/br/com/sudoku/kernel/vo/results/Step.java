package br.com.sudoku.kernel.vo.results;

import br.com.sudoku.kernel.vo.Position;

/**
 * Created by @GrupoAlpha on 22/05/15.
 */
public class Step {

    private int      step;
    private Position position;
    private int      value;

    public Step(int step, Position position, int value) {
        this.step = step;
        this.position = position;
        this.value = value;
    }

    public Step() {
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Step step = (Step) o;

        return !(position != null ? !position.equals(step.position) : step.position != null);

    }

    @Override
    public int hashCode() {
        return position != null ? position.hashCode() : 0;
    }
}
