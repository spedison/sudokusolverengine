package br.com.sudoku.kernel.vo.results;

import br.com.sudoku.kernel.vo.Game;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by @GrupoAlpha on 24/05/15.
 */
public class Solution implements Cloneable, Serializable{

    private Game game;
    private List<Step> stepList;
    private int steps;

    private boolean endGame;

    public Solution(){
        stepList = new LinkedList<>();
        endGame = false;
        steps = 0;
        game = null;
    }

    public Solution(Game game) {
        this.game = game;
        stepList = new LinkedList<>();
        endGame = false;
        steps = 0;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public List<Step> getStepList() {
        return stepList;
    }

    public void setStepList(List<Step> stepList) {
        this.stepList = stepList;
    }

    public boolean isEndGame() {
        return endGame;
    }

    public void setEndGame(boolean endGame) {
        this.endGame = endGame;
    }


    public int getSteps() {
        return steps;
    }

    public void incStep(){
        steps++;
    }

    public Solution clone (){
        Solution ret = new Solution();
        ret.setGame(game.clone());
        ret.getStepList().addAll(stepList);
        ret.steps = steps;
        return ret;
    }
}
