/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sudoku.kernel.vo;

import br.com.sudoku.kernel.interfaces.Consts;
import br.com.sudoku.kernel.vo.results.BoxResult;

import java.io.Serializable;


/**
 * This class refer to Set of cells, this group of cells test cases.
 *
 * @author GrupoAlpha
 */
public class BoxCell implements Serializable, Cloneable {

    private Cell[][] cels;

    public BoxCell() {
        cels = new Cell[Consts.SIZE_OF_BOX][Consts.SIZE_OF_BOX];
        Position pos = new Position();
        for (; pos.getCol() < cels.length; pos.setCol(pos.getCol() + 1)) {
            for (pos.setRow(0); pos.getRow() < cels[0].length; pos.setRow(pos.getRow() + 1)) {
                cels[pos.getRow()][pos.getCol()] = new Cell();
                cels[pos.getRow()][pos.getCol()].setPos(pos);
            }
        }
    }

    public Cell getCell(Position pos) {
        return cels[pos.getRow()][pos.getCol()];
    }

    public Cell getCell(int row, int col) {
        return cels[row][col];
    }

    public boolean uniqueValueInBox(BoxResult boxResult) {
        for (int value = 0; value < Consts.NUMBER_OF_SIMBOLS; value++) {
            if (uniqueValueInBox(value, boxResult.getPos())) {
                boxResult.setValue(value);
                boxResult.setResult(true);
                return true;
            }
        }
        return false;
    }

    public boolean uniqueValueInBox(int value, Position pos) {
        int count = 0;
        for (int row = 0; row < cels.length; row++) {
            for (int col = 0; col < cels[0].length; col++) {

                if (getCell(row, col).checkPossibity(value)) {
                    count++;
                    pos.setCol(col);
                    pos.setRow(row);
                }
                if (count > 1) {
                    return false;
                }
            }
        }
        return true;
    }

    public void removePossibility(int value) {
        Position currentPos = new Position();

        for (currentPos.setCol(0); currentPos.getCol() < cels[0].length; currentPos.setCol(currentPos.getCol() + 1)) {
            for (currentPos.setRow(0); currentPos.getRow() < cels.length; currentPos.setRow(currentPos.getRow() + 1)) {
                getCell(currentPos).removePossibity(value);
            }

        }
    }

    public BoxCell clone() {
        BoxCell boxCell = new BoxCell();
        boxCell.copy(this);
        return boxCell;
    }

    public void copy(final BoxCell boxCell) {
        for (int row = 0; row < cels.length; row++) {
            for (int col = 0; col < cels[0].length; col++) {
                Cell cellOrig = boxCell.getCell(row, col);
                Cell cellDest = getCell(row, col);
                cellDest.copy(cellOrig);
            }
        }
    }

}
