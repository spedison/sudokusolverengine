package br.com.sudoku.kernel.vo.groupelements;

import br.com.sudoku.kernel.interfaces.Consts;
import br.com.sudoku.kernel.vo.BoxCell;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by @GrupoAlpha on 24/05/15.
 */
public class CellCols extends ArrayList<GroupBase> implements Serializable {

    public CellCols(BoxCell[][] all) {
        for(int col = 0; col < (Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME); col++){
            add(new GroupBase(all, col, GroupBase.TypeGroup.COLUMN));
        }
    }

    public void removePossibility(int col, int value){
        get(col).removePossibility(value);
    }
}
