package br.com.sudoku.kernel.vo.groupelements;

import br.com.sudoku.kernel.interfaces.Consts;
import br.com.sudoku.kernel.vo.BoxCell;
import br.com.sudoku.kernel.vo.results.BoxResult;
import br.com.sudoku.kernel.vo.Cell;
import br.com.sudoku.kernel.vo.Position;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by @GrupoAlpha on 24/5/15.
 */
public class GroupBase extends ArrayList<Cell> {

    private int number;
    static private Logger logger = Logger.getLogger(GroupBase.class);

    public enum TypeGroup {COLUMN, ROW, GROUP};

    private TypeGroup typeGroup;

    public GroupBase(BoxCell[][] all, int number, TypeGroup typeGroup) {

        assert typeGroup.equals(TypeGroup.COLUMN) || typeGroup.equals(TypeGroup.ROW);

        this.number = number;
        this.typeGroup = typeGroup;

        int boxRow = 0; int boxCol = 0;
        int rowPos = 0; int colPos = 0;

        for (int pos = 0; pos < (Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME); pos++) {

            if(typeGroup.equals(TypeGroup.COLUMN)) {
                boxRow = pos / Consts.SIZE_OF_BOX;
                boxCol = number / Consts.SIZE_OF_BOX;
                rowPos = pos % Consts.SIZE_OF_BOX;
                colPos = number % Consts.SIZE_OF_BOX;
            }
            else if(typeGroup.equals(TypeGroup.ROW)){
                boxRow = number / Consts.SIZE_OF_BOX;
                boxCol = pos / Consts.SIZE_OF_BOX;
                rowPos = number % Consts.SIZE_OF_BOX;
                colPos = pos % Consts.SIZE_OF_BOX;
            }

            add(all[boxRow][boxCol].getCell(rowPos, colPos));
        }
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void removePossibility(int value) {
        this.forEach(cell -> cell.removePossibity(value));
    }

    public void uniqueValue(List<BoxResult> boxResult) {
        Position p = new Position();
        for (int value = 0; value < Consts.NUMBER_OF_SIMBOLS; value++) {
            if (uniqueValue(value, p, typeGroup)) {
                boxResult.add(new BoxResult(p.clone(), value));
            }
        }
    }

    public boolean uniqueValue(BoxResult boxResult) {
        boxResult.setResult(false);
        for (int value = 0; value < Consts.NUMBER_OF_SIMBOLS; value++) {
            if (uniqueValue(value, boxResult.getPos(), typeGroup)) {
                boxResult.setValue(value);
                boxResult.setResult(true);
                return true;
            }
        }
        return false;
    }

    public boolean uniqueValue(int value, Position pos, TypeGroup typeGroup) {

        int count = 0;
        int posResult = -1;
        int currentPos = 0;

        for (Cell item : this) {

            if (item.checkPossibity(value)) {
                posResult = currentPos;
                count++;
            }

            currentPos++;

            if (count > 1)
                return false;
        }

        if(count==1){

            if(typeGroup.equals(TypeGroup.COLUMN)) {
                pos.setCol(getNumber());
                pos.setRow(posResult);
            } else if(typeGroup.equals(TypeGroup.ROW)){
                pos.setRow(getNumber());
                pos.setCol(posResult);
            } else if(typeGroup.equals(TypeGroup.GROUP)){
                // TODO: Fazer a organização de um grupo para remover o array fixo!!!

            }

            logger.debug("uniqueValue found : pos=" + pos + " Tipo=" + typeGroup + " Valor=" + value);
            return true;
        }

        return false;

    }

    @Override
    public String toString() {

        String out = "Grupo:["+typeGroup+"]#" + this.getNumber() + "{";

        for(Cell c : this){
            out += c.toString();
            out += ";";
        }

        out += "}";

        return out;
    }
}
