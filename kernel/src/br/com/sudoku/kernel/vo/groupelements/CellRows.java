package br.com.sudoku.kernel.vo.groupelements;

import br.com.sudoku.kernel.interfaces.Consts;
import br.com.sudoku.kernel.vo.BoxCell;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by @GrupoAlpha on 24/05/15.
 */
public class CellRows extends ArrayList<GroupBase> implements Serializable {

    public CellRows(BoxCell[][] all) {
        for (int row = 0; row < (Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME); row++) {
            add(new GroupBase(all, row, GroupBase.TypeGroup.ROW));
        }
    }

    public void removePossibility(int row, int value) {
        get(row).removePossibility(value);
    }

    @Override
    public String toString() {

        StringBuffer out = new StringBuffer();

        this.stream().forEach(cells -> {
            out.append("\n");
            out.append(cells.toString());
        });

        out.append("\n");
        return out.toString();
    }
}
