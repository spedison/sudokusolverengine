/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sudoku.kernel.vo;

import java.io.Serializable;

/**
 * @author @GrupoAlpha
 */
public class Position implements Cloneable, Serializable {

    private int col;
    private int row;

    public Position() {
        col = 0;
        row = 0;
    }

    public Position(int row, int col) {
        this.col = col;
        this.row = row;
    }

    /**
     * @return the line
     */
    public int getCol() {
        return col;
    }

    /**
     * @param line the line to set
     */
    public void setCol(int col) {
        this.col = col;
    }

    /**
     * @return the row
     */
    public int getRow() {
        return row;
    }

    /**
     * @param row the row to set
     */
    public void setRow(int row) {
        this.row = row;
    }

    public Position clone() {
        Position retPos = new Position();
        retPos.copy(this);
        return retPos;
    }

    public void copy(Position position) {
        setRow(position.getRow());
        setCol(position.getCol());
    }

    @Override
    public boolean equals(Object o) {

        if (this == o)
            return true;

        if (o == null || getClass() != o.getClass())
            return false;

        Position position = (Position) o;

        if (col != position.col)
            return false;

        return row == position.row;
    }

    @Override
    public int hashCode() {
        int result = col;
        result = 31 * result + row;
        return result;
    }

    @Override
    public String toString() {
        return "(" + getRow() + "," + getCol() + ")";
    }
}
