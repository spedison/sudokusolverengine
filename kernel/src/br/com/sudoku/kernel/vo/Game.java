/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sudoku.kernel.vo;

import br.com.sudoku.kernel.interfaces.Consts;
import br.com.sudoku.kernel.vo.groupelements.CellCols;
import br.com.sudoku.kernel.vo.groupelements.CellRows;
import br.com.sudoku.kernel.vo.groupelements.GroupBase;

import java.io.Serializable;
import java.util.Arrays;

import static br.com.sudoku.kernel.interfaces.Consts.SIZE_OF_BOX;
import static br.com.sudoku.kernel.interfaces.Consts.SIZE_OF_GAME;

/**
 * This game is set of cols, rows and Boxes.
 *
 * @author @GrupoAlpha
 */
public class Game implements Serializable, Cloneable {

    private BoxCell boxCell[][];
    private CellRows cellRows;
    private CellCols cellCols;

    public Game() {

        boxCell = new BoxCell[SIZE_OF_GAME][SIZE_OF_GAME];

        for (int col = 0; col < boxCell.length; col++) {
            for (int row = 0; row < boxCell[0].length; row++) {
                boxCell[row][col] = new BoxCell();
            }
        }

        cellCols = new CellCols(boxCell);
        cellRows = new CellRows(boxCell);

    }

    public BoxCell getBoxPositionGame(int row, int col) {
        return boxCell[row / Consts.SIZE_OF_BOX][col / Consts.SIZE_OF_BOX];
    }

    public BoxCell getBoxPositionGame(Position pos) {
        return boxCell[pos.getRow() / Consts.SIZE_OF_BOX][pos.getCol() / Consts.SIZE_OF_BOX];
    }


    public BoxCell getBox(int row, int col) {
        return boxCell[row][col];
    }

    public BoxCell getBox(Position pos) {
        return boxCell[pos.getRow()][pos.getCol()];
    }

    public GroupBase getRow(int row) {
        return cellRows.get(row);
    }

    public GroupBase getCol(int col) {
        return cellCols.get(col);
    }

    public CellRows getCellRows() {
        return cellRows;
    }

    public CellCols getCellCols() {
        return cellCols;
    }


    public int getExecutions() {
        int row, col, count;
        count = 0;
        for (row = 0; row < (SIZE_OF_BOX * SIZE_OF_GAME); row++) {
            for (col = 0; col < (SIZE_OF_BOX * SIZE_OF_GAME); col++) {
                Cell cell = getRow(row).get(col);
                if (!cell.hasValue()) {
                    count++;
                }
            }
        }
        return count;
    }

    public Game clone() {
        Game ret = new Game();
        ret.copy(this);
        return ret;
    }

    public void copy(final Game game) {
        for (int row = 0; row < Consts.SIZE_OF_GAME; row++) {
            for (int col = 0; col < Consts.SIZE_OF_GAME; col++) {
                final BoxCell boxCellSource = game.getBox(row, col);
                BoxCell boxCellDest = this.getBox(row, col);
                boxCellDest.copy(boxCellSource);
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Game game = (Game) o;

        if (o == this)
            return true;

        for (int row = 0; row < Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME; row++) {

            for (int col = 0; col < Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME; col++) {

                if (game.getRow(row).get(col).getValue() != this.getRow(row).get(col).getValue())
                    return false;
            }
        }
        return true;

    }

    @Override
    public int hashCode() {
        int result = Arrays.deepHashCode(boxCell);
        result = 31 * result + cellRows.hashCode();
        result = 31 * result + cellCols.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Game{" +
                "cellRows=" + cellRows +
                '}';
    }
}
