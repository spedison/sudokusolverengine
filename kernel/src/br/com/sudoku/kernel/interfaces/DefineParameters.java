package br.com.sudoku.kernel.interfaces;

/**
 * Created by @GrupoAlpha on 02/06/15.
 */
public interface DefineParameters {
    default public void setProperty(String name, String value) {
    }

    default public void setProperty(String name, int value) {
    }

    default public void setProperty(String name, long value) {
    }

    default public void setProperty(String name, double value) {
    }

    default public void setProperty(String name, float value) {
    }
    default public void setProperty(String name, Object value) {
    }
}
