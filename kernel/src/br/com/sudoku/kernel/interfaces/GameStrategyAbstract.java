package br.com.sudoku.kernel.interfaces;

import br.com.sudoku.kernel.vo.Cell;
import br.com.sudoku.kernel.vo.Game;
import br.com.sudoku.kernel.vo.Position;
import br.com.sudoku.kernel.vo.results.BoxResult;
import br.com.sudoku.kernel.vo.results.Solution;
import br.com.sudoku.kernel.vo.results.Step;

import java.util.List;


/**
 * File GameStrategyAbstract.java created by @GrupoAlpha on 29/05/2015 at 21:53 for projetct SudokuSolverEngine.
 */
public abstract class GameStrategyAbstract {

    protected Game game;
    private int stepValue;

    public GameStrategyAbstract(Game game) {
        this.game = game;
        stepValue = 0;
    }

    public GameStrategyAbstract() {
        game = null;
        stepValue = 0;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public void cleanGame() {
        // Clean game and possibilities....
        game.getCellRows().parallelStream().
                flatMap(r -> r.stream()).
                forEach(cell -> {
                    cell.addAllPossibities();
                    cell.setFixedValue(false);
                    cell.setValue(Consts.INVALID_VALUE);
                });
    }

    public void resetGame() {

        // Clean game and possibilities....
        game.getCellRows().parallelStream().
                flatMap(r -> r.stream()).
                forEach(cell -> {
                    if (cell.isFixedValue()) {
                        cell.removeAllPossibilities();
                    } else {
                        cell.setValue(Consts.INVALID_VALUE);
                        cell.addAllPossibities();
                    }
                });

        // Ajust possibilities...
        for (int row = 0; row < Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME; row++) {
            for (int col = 0; col < Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME; col++) {
                Cell c = game.getRow(row).get(col);
                if (c.isFixedValue()) {
                    game.getCellRows().removePossibility(row, c.getValue());
                    game.getCellCols().removePossibility(col, c.getValue());
                    game.getBoxPositionGame(row, col).removePossibility(c.getValue());
                }
            }
        }
    }

    abstract public boolean getUniqueValueRow(BoxResult boxResult);

    abstract public boolean getUniqueValueCol(BoxResult boxResult);

    abstract public boolean getUniqueValueBox(BoxResult boxResult);

    abstract public boolean findUniqueValue(BoxResult boxResult);

    // Return true if do not find value at row.
    abstract public boolean canApplyResultRow(BoxResult boxResult);


    // Return true if do not find value at row.
    abstract public boolean canApplyResultCol(BoxResult boxResult);


    // Return true if do not find value at row.
    abstract public boolean canApplyResultBox(BoxResult boxResult);

    abstract public boolean errorInGame();

    abstract public boolean endOfGame();

    public boolean writeResult(BoxResult boxResult, Solution solution) {

        if (writeResult(boxResult, solution.getStepList())) {
            solution.incStep();
            return true;
        }

        return false;
    }

    public boolean writeResult(BoxResult boxResult, List<Step> stepList) {

        if (!writeResult(boxResult))
            return false;

        stepList.add(new Step(stepValue, boxResult.getPos().clone(), boxResult.getValue()));

        return true;
    }

    public boolean writeResult(BoxResult boxResult) {

        if (!boxResult.isResult())
            return false;

        boolean test = canApplyResultBox(boxResult);
        test &= canApplyResultCol(boxResult);
        test &= canApplyResultRow(boxResult);

        if (!test)
            return false;

        // Set Value
        game.getCol(boxResult.getPos().getCol()).get(boxResult.getPos().getRow()).setValue(boxResult.getValue());

        // Remove possibilities.... Row, Col and Box.
        game.getRow(boxResult.getPos().getRow()).removePossibility(boxResult.getValue());
        game.getCol(boxResult.getPos().getCol()).removePossibility(boxResult.getValue());
        game.getBoxPositionGame(boxResult.getPos()).removePossibility(boxResult.getValue());
        return true;
    }

    public void copy(Game gameFilled, List<Position> positions) {

        // Ajust all possibilities....
        game.getCellRows().parallelStream().
                flatMap(r -> r.stream()).
                forEach(cell -> {
                    cell.addAllPossibities();
                });

        BoxResult result = new BoxResult();
        result.setResult(true);

        positions.forEach(position -> {
            result.getPos().copy(position);
            int val = gameFilled.getCol(position.getCol()).get(position.getRow()).getValue();
            result.setValue(val);
            writeResult(result);
            game.getCol(position.getCol()).get(position.getRow()).setFixedValue(true);
        });

    }

    public void copy(Game gameFilled) {

        BoxResult result = new BoxResult();
        result.setResult(true);

        for (int row = 0; row < Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME; row++) {
            for (int col = 0; col < Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME; col++) {

                result.getPos().setCol(col);
                result.getPos().setRow(row);

                int val = gameFilled.getCol(col).get(row).getValue();
                result.setValue(val);

                writeResult(result);
            }

        }
    }
}
