/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sudoku.kernel.interfaces;

import br.com.sudoku.kernel.vo.Game;
import br.com.sudoku.kernel.vo.results.Solution;
import br.com.sudoku.kernel.vo.results.SolutionList;

/**
 * @author @GrupoAlpha
 */
public interface PerformGameInterface {

    public void execute(Solution solution);

    public void execute(Game game, SolutionList solutionList);

    public void setStepInterface(StepExecute stepExecuteInterface);

    default public int getMaxSolution() {
        return 10;
    }

    default public void setMaxSolutions(int maxSolutions){}

    public void setGameStrategy(GameStrategyAbstract gameStrategy);

    public Solution getSolution();

    public SolutionList getSolutions();

}
