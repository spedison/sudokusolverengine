package br.com.sudoku.kernel.interfaces;

/**
 * File LevelGameType.java created by @GrupoAlpha on 11/06/2015 at 10:03 for projetct SudokuSolverEngine.
 */
public enum LevelGameType {
    VERY_EASY(90, 95, "TÔ TINTINO NADA"),
    EASY(75, 80, "PELO MENOS EU TENTO"),
    MEDIUM(60, 70, "NERD TETUDO"),
    HARD(50, 60, "AH, EU TÔ MALUCO"),
    VERY_HARD(40, 50, "CHUCK NORRIS"),;

    private int minPercentItens;
    private int maxPercentItens;
    private String prettyName;

    LevelGameType(int minPercentItens, int maxPercentItens, String prettyName) {
        this.minPercentItens = minPercentItens;
        this.maxPercentItens = maxPercentItens;
        this.prettyName = prettyName;
    }

    public int getMinPercentItens() {
        return minPercentItens;
    }

    public int getMaxPercentItens() {
        return maxPercentItens;
    }

    public String getPrettyName() {
        return prettyName;
    }
}
