/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sudoku.kernel.interfaces;

import br.com.sudoku.kernel.vo.Game;

/**
 * @author @GrupoAlpha
 */
public interface LoaderGameInterface extends DefineParameters{

    boolean load(Game game);


}
