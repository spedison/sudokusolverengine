package br.com.sudoku.kernel.interfaces;

import br.com.sudoku.kernel.vo.Game;
import br.com.sudoku.kernel.vo.results.Step;

/**
 * Created by @GrupoAlpha on 22/05/15.
 */
public interface StepExecute {
    void processedOneStep(Step step, Game game);
}
