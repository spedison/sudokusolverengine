/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sudoku.kernel.interfaces;

/**
 *
 * @author @GrupoAlpha
 */
public interface Consts {
    public final int SIZE_OF_BOX = 3;
    public final int SIZE_OF_GAME = 3;
    public final int NUMBER_OF_SIMBOLS = 9;
    public final String SIMBOLS[] = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
    public final int INVALID_VALUE = -1;
    public final String INVALID_SIMBOL = "*";
}
