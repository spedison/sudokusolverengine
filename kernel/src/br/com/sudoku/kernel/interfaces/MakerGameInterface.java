package br.com.sudoku.kernel.interfaces;

import br.com.sudoku.kernel.vo.Game;

/**
 * File MakerGameInterface.java created by @GrupoAlpha on 27/05/2015 at 17:47 for projetct SudokuSolverEngine.
 */

public interface MakerGameInterface {



    public Game makeGame(GameStrategyAbstract gameStrategyAbstract, LevelGameType levelGameType);
}
