/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sudoku.kernel.printers;

import br.com.sudoku.kernel.interfaces.Consts;
import br.com.sudoku.kernel.interfaces.PrinterGameInterface;
import br.com.sudoku.kernel.vo.Cell;
import br.com.sudoku.kernel.vo.Game;

/**
 *
 * @author @GrupoAlpha
 */
public class PrinterGame implements PrinterGameInterface {

    @Override
    public void print(Game game) {

        for (int row = 0; row < (Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME); row++) {
            String firstLine = "";
            String secondLine = "";
            String strChar;

            for (int col = 0; col < (Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME); col++) {
                Cell cell = game.getCol(col).get(row);

                if ((col + 1) % 3 == 0) {
                    strChar = " | ";
                } else {
                    strChar = "   ";
                }

                if (cell.hasValue()) {
                    if (cell.isFixedValue()) {
                        firstLine += String.format("%5S[%2S]%s", " ", Consts.SIMBOLS[cell.getValue()], strChar);
                    } else {
                        firstLine += String.format("%9S%s", Consts.SIMBOLS[cell.getValue()], strChar);
                    }
                } else {
                    firstLine += String.format("%9S%s", Consts.INVALID_SIMBOL, strChar);

                }

                for (int check = 0; check < Consts.NUMBER_OF_SIMBOLS; check++) {
                    if (cell.checkPossibity(check)) {
                        secondLine += String.format("%S", Consts.SIMBOLS[check]);
                    } else {
                        secondLine += " ";
                    }
                }
                secondLine += strChar;
            }

            System.out.println(firstLine);
            System.out.println(secondLine);
            if ((((row + 1) % 3) == 0)) {
                System.out.println("-----------------------------------------------------------------------------------------------------------");
            }

        }
        System.out.println("***********************************************************************************************************");
    }
}
