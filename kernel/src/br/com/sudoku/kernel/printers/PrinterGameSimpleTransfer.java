/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sudoku.kernel.printers;

import br.com.sudoku.kernel.interfaces.Consts;
import br.com.sudoku.kernel.interfaces.PrinterGameInterface;
import br.com.sudoku.kernel.vo.Cell;
import br.com.sudoku.kernel.vo.Game;
import br.com.sudoku.kernel.vo.groupelements.GroupBase;

import java.io.PrintWriter;

/**
 * @author @GrupoAlpha
 */
public class PrinterGameSimpleTransfer implements PrinterGameInterface {

    PrintWriter out;
    int tipo;

    static final public String OUT = "out";
    static final public String TIPO = "tipo";
    static final public int DADO = 1;
    static final public int POSSIBILIDADES = 2;
    static final public int ALL = 3;


    @Override
    public void setProperty(String name, Object value) {
        if (name.equalsIgnoreCase(OUT)) {
            out = (PrintWriter) value;
        }
    }

    @Override
    public void setProperty(String name, int value) {
        if (name.equalsIgnoreCase(TIPO)) {
            tipo = value;
        }
    }

    @Override
    public void print(Game game) {

        if (tipo == DADO) {
            printData(game);
        }

        if (tipo == POSSIBILIDADES) {
            printPossibilities(game);
        }

        if (tipo == ALL) {
            printData(game);
            out.println();
            printPossibilities(game);
        }

    }

    private void printData(Game game) {
        out.print("DATA:");
        for (GroupBase row : game.getCellRows()) {
            for (Cell cell : row) {
                if (cell.isFixedValue()|cell.hasValue())
                    out.print(Consts.SIMBOLS[cell.getValue()]);
                else
                    out.print("*");
            }
        }
        out.println("");
    }

    private void printPossibilities(Game game) {
        out.print("POSSIBILITIES:");
        for (GroupBase row : game.getCellRows()) {
            for (Cell cell : row) {
                if (cell.isFixedValue() | cell.hasValue())
                    out.print(";");
                else {
                    for (int bol : cell.getPossibitiesInt())
                        out.print(bol);
                    out.print(";");
                }
            }
        }
    }
}

