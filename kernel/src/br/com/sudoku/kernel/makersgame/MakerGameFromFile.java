package br.com.sudoku.kernel.makersgame;

import br.com.sudoku.kernel.interfaces.Consts;
import br.com.sudoku.kernel.interfaces.GameStrategyAbstract;
import br.com.sudoku.kernel.interfaces.LevelGameType;
import br.com.sudoku.kernel.interfaces.MakerGameInterface;
import br.com.sudoku.kernel.loadersgame.LoaderFileTextToSolve;
import br.com.sudoku.kernel.vo.Game;
import br.com.sudoku.kernel.vo.Position;

import java.util.Random;

/**
 * File MakerGame.java created by @GrupoAlpha on 27/05/2015 at 17:55 for projetct SudokuSolverEngine.
 */
public class MakerGameFromFile implements MakerGameInterface {

    private Random rnd;
    private GameStrategyAbstract gameStrategy;
    private LoaderFileTextToSolve loaderFileTextToSolve;

    public MakerGameFromFile() {
        rnd = new Random(System.currentTimeMillis());
        loaderFileTextToSolve = new LoaderFileTextToSolve();
    }

    private int getRndBetween(int min, int max) {
        int range = max - min;
        int val = rnd.nextInt(range + 1);
        return val + min;
    }

    private void getRndPositionBetween(Position pos, int min, int max) {
        pos.setCol(getRndBetween(min, max));
        pos.setRow(getRndBetween(min, max));
    }

    private void getRndPositionBetween(Position pos) {
        int max = Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME - 1;
        getRndPositionBetween(pos, 0, max);
    }

    @Override
    public Game makeGame(GameStrategyAbstract gameStrategy, LevelGameType type) {
        int min = 0;
        int max = 0;

        this.gameStrategy = gameStrategy;

        min = (int) (type.getMinPercentItens() * Math.pow(Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME, 2)) / 100;
        max = (int) (type.getMaxPercentItens() * Math.pow(Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME, 2)) / 100;

        loaderFileTextToSolve.setProperty(LoaderFileTextToSolve.POINTS_DEFINED, getRndBetween(min, max));
        loaderFileTextToSolve.load(gameStrategy.getGame());
        System.out.println("Profile is:" + type);
        return gameStrategy.getGame();
    }

/*
    private Game makeGame(int min, int max) {

        Game ret = new Game();

        gameStrategy.setGame(ret);

        BoxResult br = new BoxResult();

        // Get a quantity of points based on profile game.
        int quantity = getRndBetween(min, max);
        List<Step> stepList = new LinkedList<>();

        // Kicking points... and values. This step is start of process.
        for (int count = 0; count < ((quantity > 10) ? 10 : quantity); count++) {

            br.reset();

            getRndPositionBetween(br.getPos(), 0, Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME - 1);

            Cell c = ret.getCol(br.getPos().getCol()).get(br.getPos().getRow());

            if (c.hasValue() || c.isFixedValue()) {
                System.out.print("-");
                count--;
                continue;
            }

            ArrayList<Integer> possibilities = c.getPossibitiesInt();
            if (possibilities.size() == 0) {
                count--;
                System.out.print("-");
                continue;
            } else if (possibilities.size() == 1) {
                br.setValue(possibilities.get(0));
            } else {
                br.setValue(possibilities.get(getRndBetween(0, possibilities.size() - 1)));
            }
            br.setResult(true);

            if (!gameStrategy.writeResult(br, stepList)) {
                count--;
                System.out.print("-");
            } else {
                c.setFixedValue(true);
                System.out.print("+");
            }
        }

        SolutionList solutions = new SolutionList();
        PerformOneGameList performOneGameList = new PerformOneGameList();
        performOneGameList.setGameStrategy(gameStrategy);

        // If the number of points is more than 10 points... solve it for find complete and valid game.
        if (quantity >= 10) {
            performOneGameList.execute(gameStrategy.getGame(), solutions);
            System.out.println("\n\nProcessing Solution\n\n");
        }

        // Get one valid game and set only rest point as attached points.
        for (Solution solItem : solutions) {

            // Set game to gameStrategy.
            gameStrategy.setGame(solItem.getGame());

            // If game is bad.
            if (gameStrategy.errorInGame())
                continue;

            // Process rest of the points.
            for (int x = 10; x < quantity; x++) {
                Position position = new Position();
                getRndPositionBetween(position);
                Cell c = gameStrategy.getGame().getRow(position.getRow()).get(position.getCol());
                if (c.isFixedValue())
                    x--;
                else
                    c.setFixedValue(true);
            }
            System.out.println("The game is found\n");
            // Ok, clean the solution game and return.
            gameStrategy.resetGame();
            return gameStrategy.getGame();
        }
        // So sorry, I can't found the game.
        return null;
    }*/
}
