package br.com.sudoku.kernel.makersgame;

import br.com.sudoku.kernel.interfaces.Consts;
import br.com.sudoku.kernel.interfaces.GameStrategyAbstract;
import br.com.sudoku.kernel.interfaces.LevelGameType;
import br.com.sudoku.kernel.interfaces.MakerGameInterface;
import br.com.sudoku.kernel.solvers.PerformOneGameList;
import br.com.sudoku.kernel.vo.Cell;
import br.com.sudoku.kernel.vo.Game;
import br.com.sudoku.kernel.vo.Position;
import br.com.sudoku.kernel.vo.results.BoxResult;
import br.com.sudoku.kernel.vo.results.Solution;
import br.com.sudoku.kernel.vo.results.SolutionList;
import br.com.sudoku.kernel.vo.results.Step;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * File MakerGame.java created by @GrupoAlpha on 27/05/2015 at 17:55 for projetct SudokuSolverEngine.
 */
public class MakerGame implements MakerGameInterface {

    private Random rnd;
    private GameStrategyAbstract gameStrategy;

    public MakerGame() {
        rnd = new Random(System.currentTimeMillis());
    }

    private int getRndBetween(int min, int max) {
        int range = max - min;
        int val = rnd.nextInt(range + 1);
        return val + min;
    }

    private void getRndPositionBetween(Position pos, int min, int max) {
        pos.setCol(getRndBetween(min, max));
        pos.setRow(getRndBetween(min, max));
    }

    private void getRndPositionBetween(Position pos) {
        int max = Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME - 1;
        getRndPositionBetween(pos, 0, max);
    }

    @Override
    public Game makeGame(GameStrategyAbstract gameStrategy, LevelGameType type) {

        int min = 0;
        int max = 0;

        this.gameStrategy = gameStrategy;

        max = (int) ((type.getMaxPercentItens() * Math.pow(Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME, 2)) / 100);
        min = (int) ((type.getMinPercentItens() * Math.pow(Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME, 2)) / 100);

        System.out.println("Profile is:" + type);
        return makeGame(min, max);
    }

    private Game makeGame(int min, int max) {

        Game ret = new Game();

        gameStrategy.setGame(ret);

        BoxResult br = new BoxResult();

        // Get a quantity of points based on profile game.
        int quantity = getRndBetween(min, max);
        List<Step> stepList = new LinkedList<>();

        // Kicking points... and values. This step is start of process.
        for (int count = 0; count < ((quantity > 10) ? 10 : quantity); count++) {

            br.reset();

            getRndPositionBetween(br.getPos(), 0, Consts.SIZE_OF_BOX * Consts.SIZE_OF_GAME - 1);

            Cell c = ret.getCol(br.getPos().getCol()).get(br.getPos().getRow());

            if (c.hasValue() || c.isFixedValue()) {
                count--;
                continue;
            }

            ArrayList<Integer> possibilities = c.getPossibitiesInt();
            if (possibilities.size() == 0) {
                count--;
                continue;
            } else if (possibilities.size() == 1) {
                br.setValue(possibilities.get(0));
            } else {
                br.setValue(possibilities.get(getRndBetween(0, possibilities.size() - 1)));
            }
            br.setResult(true);

            if (!gameStrategy.writeResult(br, stepList)) {
                count--;
            } else {
                c.setFixedValue(true);
            }
        }

        SolutionList solutions = new SolutionList();
        PerformOneGameList performOneGameList = new PerformOneGameList();
        performOneGameList.setGameStrategy(gameStrategy);

        // If the number of points is more than 10 points... solve it for find complete and valid game.
        if (quantity >= 10) {
            performOneGameList.execute(gameStrategy.getGame(), solutions);
        }

        // Get one valid game and set only rest point as attached points.
        for (Solution solItem : solutions) {

            // Set game to gameStrategy.
            gameStrategy.setGame(solItem.getGame());

            // If game is bad.
            if (gameStrategy.errorInGame())
                continue;

            // Process rest of the points.
            for (int x = 10; x < quantity; x++) {
                Position position = new Position();
                getRndPositionBetween(position);
                Cell c = gameStrategy.getGame().getRow(position.getRow()).get(position.getCol());
                if (c.isFixedValue())
                    x--;
                else
                    c.setFixedValue(true);
            }
            System.out.println("The game is found\n");
            // Ok, clean the solution game and return.
            gameStrategy.resetGame();
            return gameStrategy.getGame();
        }
        // So sorry, I can't found the game.
        return null;
    }
}
