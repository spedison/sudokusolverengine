package br.com.sudoku.database.vo;

import javax.persistence.*;

/**
 * Created by @GrupoAlpha on 24/05/15.
 */
@Entity
@Table(name = "tb_user_game", schema = "", catalog = "sudoku")
public class UserGame {
    private int userGameId;
    private int tbUserUserId;
    private int tbGameGameId;

    @Id
    @Column(name = "user_game_id")
    public int getUserGameId() {
        return userGameId;
    }

    public void setUserGameId(int userGameId) {
        this.userGameId = userGameId;
    }

    @Basic
    @Column(name = "tb_user_user_id")
    public int getTbUserUserId() {
        return tbUserUserId;
    }

    public void setTbUserUserId(int tbUserUserId) {
        this.tbUserUserId = tbUserUserId;
    }

    @Basic
    @Column(name = "tb_game_game_id")
    public int getTbGameGameId() {
        return tbGameGameId;
    }

    public void setTbGameGameId(int tbGameGameId) {
        this.tbGameGameId = tbGameGameId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserGame userGame = (UserGame) o;

        if (userGameId != userGame.userGameId) return false;
        if (tbUserUserId != userGame.tbUserUserId) return false;
        if (tbGameGameId != userGame.tbGameGameId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userGameId;
        result = 31 * result + tbUserUserId;
        result = 31 * result + tbGameGameId;
        return result;
    }
}
