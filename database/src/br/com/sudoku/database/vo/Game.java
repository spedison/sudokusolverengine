package br.com.sudoku.database.vo;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by @GrupoAlpha on 5/24/15.
 */
@Entity
@Table(name = "tb_game", schema = "", catalog = "sudoku")
public class Game {
    private int gameId;
    private Timestamp dtCreate;
    private String strGame;
    private Integer lines;
    private Integer cols;
    private String groupRows;
    private String groupCols;
    private String groupsRows;
    private String groupsCols;

    @Id
    @Column(name = "game_id")
    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    @Basic
    @Column(name = "dt_create")
    public Timestamp getDtCreate() {
        return dtCreate;
    }

    public void setDtCreate(Timestamp dtCreate) {
        this.dtCreate = dtCreate;
    }

    @Basic
    @Column(name = "str_game")
    public String getStrGame() {
        return strGame;
    }

    public void setStrGame(String strGame) {
        this.strGame = strGame;
    }

    @Basic
    @Column(name = "lines")
    public Integer getLines() {
        return lines;
    }

    public void setLines(Integer lines) {
        this.lines = lines;
    }

    @Basic
    @Column(name = "cols")
    public Integer getCols() {
        return cols;
    }

    public void setCols(Integer cols) {
        this.cols = cols;
    }

    @Basic
    @Column(name = "group_rows")
    public String getGroupRows() {
        return groupRows;
    }

    public void setGroupRows(String groupRows) {
        this.groupRows = groupRows;
    }

    @Basic
    @Column(name = "group_cols")
    public String getGroupCols() {
        return groupCols;
    }

    public void setGroupCols(String groupCols) {
        this.groupCols = groupCols;
    }

    @Basic
    @Column(name = "groups_rows")
    public String getGroupsRows() {
        return groupsRows;
    }

    public void setGroupsRows(String groupsRows) {
        this.groupsRows = groupsRows;
    }

    @Basic
    @Column(name = "groups_cols")
    public String getGroupsCols() {
        return groupsCols;
    }

    public void setGroupsCols(String groupsCols) {
        this.groupsCols = groupsCols;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Game game = (Game) o;

        if (gameId != game.gameId) return false;
        if (dtCreate != null ? !dtCreate.equals(game.dtCreate) : game.dtCreate != null) return false;
        if (strGame != null ? !strGame.equals(game.strGame) : game.strGame != null) return false;
        if (lines != null ? !lines.equals(game.lines) : game.lines != null) return false;
        if (cols != null ? !cols.equals(game.cols) : game.cols != null) return false;
        if (groupRows != null ? !groupRows.equals(game.groupRows) : game.groupRows != null) return false;
        if (groupCols != null ? !groupCols.equals(game.groupCols) : game.groupCols != null) return false;
        if (groupsRows != null ? !groupsRows.equals(game.groupsRows) : game.groupsRows != null) return false;
        if (groupsCols != null ? !groupsCols.equals(game.groupsCols) : game.groupsCols != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = gameId;
        result = 31 * result + (dtCreate != null ? dtCreate.hashCode() : 0);
        result = 31 * result + (strGame != null ? strGame.hashCode() : 0);
        result = 31 * result + (lines != null ? lines.hashCode() : 0);
        result = 31 * result + (cols != null ? cols.hashCode() : 0);
        result = 31 * result + (groupRows != null ? groupRows.hashCode() : 0);
        result = 31 * result + (groupCols != null ? groupCols.hashCode() : 0);
        result = 31 * result + (groupsRows != null ? groupsRows.hashCode() : 0);
        result = 31 * result + (groupsCols != null ? groupsCols.hashCode() : 0);
        return result;
    }
}
