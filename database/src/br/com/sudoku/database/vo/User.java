package br.com.sudoku.database.vo;

import javax.persistence.*;

/**
 * Created by @GrupoAlpha on 5/24/15.
 */
@Entity
@Table(name = "tb_user", schema = "", catalog = "sudoku")
public class User {
    private int userId;
    private String nome;
    private String email;
    private String senha;

    @Id
    @Column(name = "user_id")
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "nome")
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "senha")
    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (userId != user.userId) return false;
        if (nome != null ? !nome.equals(user.nome) : user.nome != null) return false;
        if (email != null ? !email.equals(user.email) : user.email != null) return false;
        if (senha != null ? !senha.equals(user.senha) : user.senha != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + (nome != null ? nome.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (senha != null ? senha.hashCode() : 0);
        return result;
    }
}
