package br.com.sudoku.database.vo;

import javax.persistence.*;

/**
 * Created by @GrupoAlpha on 5/24/15.
 */
@Entity
@Table(name = "tb_failed_step", schema = "", catalog = "sudoku")
@IdClass(FailedStepPK.class)
public class FailedStep {
    private int failedStepId;
    private Integer row;
    private Integer col;
    private Integer value;

    @ManyToOne(optional = false)
    private Solution solution;

    @Id
    @GeneratedValue
    @Column(name = "failed_step_id")
    public int getFailedStepId() {
        return failedStepId;
    }

    public void setFailedStepId(int failedStepId) {
        this.failedStepId = failedStepId;
    }

    @Basic
    @Column(name = "row")
    public Integer getRow() {
        return row;
    }

    public void setRow(Integer row) {
        this.row = row;
    }

    @Basic
    @Column(name = "col")
    public Integer getCol() {
        return col;
    }

    public void setCol(Integer col) {
        this.col = col;
    }

    @Basic
    @Column(name = "value")
    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }


    public Solution getSolution() {
        return solution;
    }

    public void setSolution(Solution solution) {
        this.solution = solution;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FailedStep that = (FailedStep) o;

        if (failedStepId != that.failedStepId) return false;

        if (row != null ? !row.equals(that.row) : that.row != null) return false;
        if (col != null ? !col.equals(that.col) : that.col != null) return false;
        if (value != null ? !value.equals(that.value) : that.value != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = failedStepId;
        result = 31 * result + (row != null ? row.hashCode() : 0);
        result = 31 * result + (col != null ? col.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }
}
