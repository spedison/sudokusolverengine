package br.com.sudoku.database.vo;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by @GrupoAlpha on 24/05/15.
 */
@Entity
@Table(name = "tb_solution", schema = "", catalog = "sudoku")
public class Solution {
    private int solucaoId;
    private String name;
    private Timestamp startDate;
    private Timestamp endDate;
    private Integer spendTime;
    private int tbUserGameUserGameId;

    @Id
    @Column(name = "solucao_id")
    public int getSolucaoId() {
        return solucaoId;
    }

    public void setSolucaoId(int solucaoId) {
        this.solucaoId = solucaoId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "start_date")
    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    @Basic
    @Column(name = "end_date")
    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    @Basic
    @Column(name = "spend_time")
    public Integer getSpendTime() {
        return spendTime;
    }

    public void setSpendTime(Integer spendTime) {
        this.spendTime = spendTime;
    }

    @Basic
    @Column(name = "tb_user_game_user_game_id")
    public int getTbUserGameUserGameId() {
        return tbUserGameUserGameId;
    }

    public void setTbUserGameUserGameId(int tbUserGameUserGameId) {
        this.tbUserGameUserGameId = tbUserGameUserGameId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Solution solution = (Solution) o;

        if (solucaoId != solution.solucaoId) return false;
        if (tbUserGameUserGameId != solution.tbUserGameUserGameId) return false;
        if (name != null ? !name.equals(solution.name) : solution.name != null) return false;
        if (startDate != null ? !startDate.equals(solution.startDate) : solution.startDate != null) return false;
        if (endDate != null ? !endDate.equals(solution.endDate) : solution.endDate != null) return false;
        if (spendTime != null ? !spendTime.equals(solution.spendTime) : solution.spendTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = solucaoId;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        result = 31 * result + (spendTime != null ? spendTime.hashCode() : 0);
        result = 31 * result + tbUserGameUserGameId;
        return result;
    }
}
