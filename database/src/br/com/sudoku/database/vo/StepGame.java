package br.com.sudoku.database.vo;

import javax.persistence.*;

/**
 * Created by @GrupoAlpha on 24/05/15.
 */
@Entity
@Table(name = "tb_step_game", schema = "", catalog = "sudoku")
public class StepGame {
    private int stepGameId;
    private String row;
    private String col;
    private String value;
    private int tbSolucaoSolucaoId;

    @Id
    @Column(name = "step_game_id")
    public int getStepGameId() {
        return stepGameId;
    }

    public void setStepGameId(int stepGameId) {
        this.stepGameId = stepGameId;
    }

    @Basic
    @Column(name = "row")
    public String getRow() {
        return row;
    }

    public void setRow(String row) {
        this.row = row;
    }

    @Basic
    @Column(name = "col")
    public String getCol() {
        return col;
    }

    public void setCol(String col) {
        this.col = col;
    }

    @Basic
    @Column(name = "value")
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Basic
    @Column(name = "tb_solucao_solucao_id")
    public int getTbSolucaoSolucaoId() {
        return tbSolucaoSolucaoId;
    }

    public void setTbSolucaoSolucaoId(int tbSolucaoSolucaoId) {
        this.tbSolucaoSolucaoId = tbSolucaoSolucaoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StepGame stepGame = (StepGame) o;

        if (stepGameId != stepGame.stepGameId) return false;
        if (tbSolucaoSolucaoId != stepGame.tbSolucaoSolucaoId) return false;
        if (row != null ? !row.equals(stepGame.row) : stepGame.row != null) return false;
        if (col != null ? !col.equals(stepGame.col) : stepGame.col != null) return false;
        if (value != null ? !value.equals(stepGame.value) : stepGame.value != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = stepGameId;
        result = 31 * result + (row != null ? row.hashCode() : 0);
        result = 31 * result + (col != null ? col.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + tbSolucaoSolucaoId;
        return result;
    }
}
