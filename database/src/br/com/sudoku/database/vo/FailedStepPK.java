package br.com.sudoku.database.vo;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by @GrupoAlpha on 24/05/15.
 */
public class FailedStepPK implements Serializable {
    private int failedStepId;
    private int tbGameGameId;

    @Column(name = "failed_step_id")
    @Id
    public int getFailedStepId() {
        return failedStepId;
    }

    public void setFailedStepId(int failedStepId) {
        this.failedStepId = failedStepId;
    }

    @Column(name = "tb_game_game_id")
    @Id
    public int getTbGameGameId() {
        return tbGameGameId;
    }

    public void setTbGameGameId(int tbGameGameId) {
        this.tbGameGameId = tbGameGameId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FailedStepPK that = (FailedStepPK) o;

        if (failedStepId != that.failedStepId) return false;
        if (tbGameGameId != that.tbGameGameId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = failedStepId;
        result = 31 * result + tbGameGameId;
        return result;
    }
}
