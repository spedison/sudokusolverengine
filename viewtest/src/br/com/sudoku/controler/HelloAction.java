package br.com.sudoku.controler;

/**
 * Created by @GrupoAlpha on 5/23/15.
 */
public class HelloAction {
    private String name;
    private int idade;

    public String execute() throws Exception {
        return "success";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }
}
