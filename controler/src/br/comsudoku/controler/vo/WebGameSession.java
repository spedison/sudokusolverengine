package br.comsudoku.controler.vo;

import br.com.sudoku.kernel.exceptions.ErrorEndOfGame;
import br.com.sudoku.kernel.exceptions.ErrorStepNoFound;
import br.com.sudoku.kernel.exceptions.ErrorValueNotChanged;
import br.com.sudoku.kernel.interfaces.Consts;
import br.com.sudoku.kernel.interfaces.LevelGameType;
import br.com.sudoku.kernel.makersgame.MakerGameFromFile;
import br.com.sudoku.kernel.solvers.GameStrategy;
import br.com.sudoku.kernel.solvers.PerformOneGameList;
import br.com.sudoku.kernel.vo.Game;
import br.com.sudoku.kernel.vo.Position;
import br.com.sudoku.kernel.vo.results.BoxResult;
import br.com.sudoku.kernel.vo.results.ListOfSteps;
import br.com.sudoku.kernel.vo.results.SolutionList;
import br.com.sudoku.kernel.vo.results.Step;
import br.com.sudoku.utils.ArrayUtil;
import org.apache.log4j.Logger;

import java.io.Serializable;

/**
 * Created by @GrupoAlpha on 04/06/15.
 */
public class WebGameSession implements Serializable {

    // Logger
    private static Logger logger = Logger.getLogger(WebGameSession.class);

    private Game game;
    private GameStrategy gameStrategy;
    private MakerGameFromFile makerGameFromFile;
    private PerformOneGameList solverGame;
    private ListOfSteps gammerStep;
    private SolutionList solutions;

    private long lastTouch; // In miliseconds.

    public WebGameSession() {
        game = new Game();
        gameStrategy = new GameStrategy(game);
        makerGameFromFile = new MakerGameFromFile();
        solverGame = new PerformOneGameList();
        gammerStep = new ListOfSteps();
        solutions = new SolutionList();
        touch();
    }

    private void touch() {
        lastTouch = System.currentTimeMillis();
    }

    // The user have 30 minutes for touch objects...
    public boolean isDeed() {
        return (((System.currentTimeMillis() - lastTouch) / 1000) > 30 * 60);
    }

    public Game getGame() {
        touch();
        return game;
    }

    public GameStrategy getGameStrategy() {
        touch();
        return gameStrategy;
    }

    public void makeNewGame(String type) {

        touch();

        // Remove all Steps.
        gammerStep.clear();
        // Remove all to make new game.
        gameStrategy.cleanGame();
        // Set type of game
        LevelGameType _type = LevelGameType.valueOf(LevelGameType.class, type);
        // Make new game
        makerGameFromFile.makeGame(gameStrategy, _type);
        // Fill list of steps.
        try {
            gammerStep.addAllSteps(gameStrategy);
        } catch (ErrorEndOfGame errorEndOfGame) {
            logger.error("Esse jogo já está resolvido! Temos problema de lógica.");
        }
    }

    public String makeNextStep(String control, String value, String withTrick) {

        String[] itens = control.split("_");
        String pos = itens[itens.length - 1];

        if (pos.length() == 0 || pos.length() % 2 != 0) {
            logger.error("Pos com tamanho impar não pode!");
            return control + ";;1\nresult;Controle desconhecido;0";
        }

        BoxResult boResult = new BoxResult();
        boResult.getPos().setRow(Integer.parseInt(pos.substring(0, pos.length() / 2)));
        boResult.getPos().setCol(Integer.parseInt(pos.substring(pos.length() / 2)));
        boResult.setValue(ArrayUtil.indexOf(Consts.SIMBOLS, value));
        boResult.setResult(true);

        int posGammer = gammerStep.indexOf(new Step(0, boResult.getPos(), 0));
        if (posGammer == -1) {
            logger.error("Posição ou controle não localizado. Controle=" + control + "; Valor=" + value);
            return control + ";;1\nresult;Posicao não localizada! Erro...;0";
        }

        if (gammerStep.get(posGammer).getValue() != Consts.INVALID_VALUE) {
            logger.error("Controle já foi respondido!!! Controle=" + control + " ; Valor=" + value);
            return control + ";" + Consts.SIMBOLS[gammerStep.get(posGammer).getValue()] +
                    ";0\nresult;Valor já tinha sido definido;0";
        }

        // Clone the game.
        Game copyGame = getGame().clone();
        GameStrategy gm = new GameStrategy(copyGame);

        // Apply que Key (in the copy)
        if (!gm.writeResult(boResult)) {
            return control + ";;1\nresult;Valor não respeita as regras do jogo;0";
        }

        // Try solve the game (copy)
        solverGame.setMaxSolutions(1);
        solutions.clear();
        solverGame.execute(copyGame, solutions);

        // If can't solve the game, return message to user.
        if (solutions.size() == 0) {
            return "result;Não encontrei solução para esse jogo;0\n" + control + ";;1\n";
        }

        String tmpResult = control + ";" + value + ";0\n";

        // If game is solved,
        //             ...get the first empty (position)
        try {
            gammerStep.addOneStep(
                    boResult.getPos().getRow(),
                    boResult.getPos().getCol(),
                    boResult.getValue());
        } catch (ErrorValueNotChanged errorValueNotChanged) {

        } catch (ErrorStepNoFound errorStepNoFound) {

        }

        String msg;
        if (gameStrategy.endOfGame()) {
            msg = "Fim do jogo! Parabéns.";
        } else {
            msg = "Parabéns pela jogada. Continue, pois falta somente " + gammerStep.getCountEmptyFields() + " jogadas";
        }

        if (!(withTrick.compareToIgnoreCase("true") == 0 || withTrick.compareToIgnoreCase("1") == 0)) {
            return tmpResult + "result;"+msg+";0";
        }

        boolean gameIsOk = getGameStrategy().writeResult(boResult);

        //             ...so I can apply moviment in game
        if (gameIsOk) {
            if (gameStrategy.endOfGame()) {
                return tmpResult + "result;Fim de jogo. PARABÉNS!!;0";
            }
        } else {
            logger.error("Problema para adicionar o item no jogo, ferindo as regras.");
            return control + ";;1\nresult;Entrada não respeita regras básicas;0";
        }

        //             ...get empty position
        Position emptyPosition = null;

        // get empty cell.
        try {
            emptyPosition = gammerStep.getFirstEmpty();
        } catch (ErrorEndOfGame errorEndOfGame) {
            logger.error("Fim de jogo não detectado,no lugar errado!!!");
            return tmpResult + "result;Fim de jogo. PARABÉNS!!;0";
        }

        // Get value than I will aplly in empty cell.
        int valueToFill = copyGame.getRow(emptyPosition.getRow()).get(emptyPosition.getCol()).getValue();
        gameStrategy.setGame(game);
        if (!gameStrategy.writeResult(new BoxResult(emptyPosition, valueToFill))) {
            logger.error("Problemas ao adicionar o segundo item.");
        }

        // Check game !!!
        try {
            gammerStep.addOneStep(emptyPosition.getRow(), emptyPosition.getCol(), valueToFill);
        } catch (ErrorStepNoFound e) {
            logger.error("Erro de lógica ao aplicar o resultado do Bonus!!!", e);
            return tmpResult + "result;Problema na lógica do kernel;0";
        } catch (ErrorValueNotChanged errorValueNotChanged) {
            logger.error("Alerta.... está definindo 2 vezes o mesmo valor:", errorValueNotChanged);
            errorValueNotChanged.printStackTrace();
        }

        //             ...get the value in next step
        String valueRet =
                Consts.SIMBOLS[game.getRow(emptyPosition.getRow()).get(emptyPosition.getCol()).getValue()];

        //             ...Make message
        String controlName = control.replace(pos, "");
        controlName = controlName + emptyPosition.getRow() + "" + emptyPosition.getCol();

        if (gameStrategy.endOfGame()) {
            msg = "Fim do jogo! Parabéns.";
        } else {
            msg = "Parabéns pela jogada. Continue, pois falta somente " + gammerStep.getCountEmptyFields() + " jogadas";
        }

        //
        return controlName + ";" + valueRet + ";0\n" + tmpResult + "result;" + msg + ";0";
    }
}
