package br.comsudoku.controler.vo;

import java.util.Hashtable;

/**
 * File WebGameControlSession.java created by @GrupoAlpha on 12/06/2015 at 15:18 for projetct SudokuSolverEngine.
 */
public class WebGameControlSession extends Hashtable<String, WebGameSession> {

    static private WebGameControlSession session = new WebGameControlSession();
    static private long count = 0;

    static public WebGameControlSession getSession() {

        if (((count++) % 100) == 0) {
            cleanDeadItens();
        }

        return session;
    }

    private static void cleanDeadItens() {

        for (String itenKey : session.keySet()) {

            WebGameSession se = session.get(itenKey);
            if (se.isDeed()) {
                session.remove(itenKey);
                se = null;
            }

        }

    }

    private WebGameControlSession() {
    }

}
