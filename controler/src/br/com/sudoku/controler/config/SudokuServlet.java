package br.com.sudoku.controler.config;

import br.comsudoku.controler.vo.WebGameControlSession;
import br.comsudoku.controler.vo.WebGameSession;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by @Grupo Alpha on 02/06/15.
 */
public class SudokuServlet extends HttpServlet {

    static private Logger logger = Logger.getLogger(SudokuServlet.class);

    public void init() {

    }

    public void doGet(HttpServletRequest req, HttpServletResponse res) {

        // Get session control in memory (Singleton)
        WebGameControlSession wgcs = WebGameControlSession.getSession();
        WebGameSession gameSession = null;

        // Request user session.
        if (wgcs.containsKey(req.getParameter("session"))) {
            // First step, get the session user
            gameSession = wgcs.get(req.getSession().getId());
        }

        // Define the type of result.
        res.setContentType("text/html");

        try {
            // It's problem. The session user is not created.
            if (gameSession == null) {

                res.getWriter().println("NOK");
                logger.error("A sessão não foi criada ou foi perdida.");
                return;
            }

            String command = req.getParameter("cmd");

            // Check comands.

            // --- Execute echo command.
            if (command.equalsIgnoreCase("echo")) {

                res.getWriter().println(req.getParameter("value"));
                logger.debug("comando echo chamado. Valor = " + req.getParameter("value"));

            // --- Execute key command.
            } else if (command.equalsIgnoreCase("key")) {
                // Response format: ControlName;Value;Enable\nControlName;Value;Enable...
                logger.debug("Recebido   cmd KEY ( controle=" + req.getParameter("control") + ", value=" + req.getParameter("value") + " )");
                res.getWriter().println(gameSession.makeNextStep(req.getParameter("control"), req.getParameter("value"), req.getParameter("trick")));
                logger.debug("Processando cmd KEY ( controle=" + req.getParameter("control") + ", value=" + req.getParameter("value") + " )");

            } else {
                // Other comands.
                res.getWriter().println("NOK");
                logger.error("Comando não identificado.");
            }

        } catch (IOException e) {
            e.printStackTrace();
            log("Problemas ao escrever na saída", e);
        }

    }

    public void destroy() {

        log("Terminando Serlet" + getServletName());
    }
}
