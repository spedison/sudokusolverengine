package br.com.sudoku.controler.config;

import br.com.sudoku.utils.LoadLog4J;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;

/**
 * Created by @GrupoAlpha on 5/23/15.
 */

public class ConfigLog4J extends HttpServlet {

    public void init() {

        String prefix = getServletContext().getRealPath("/");
        String file = getInitParameter("log4j-init-file");

        boolean hasReadFile = false;

        if (file != null && prefix != null && !file.isEmpty() && !prefix.isEmpty()) {
            String fileToReadString = prefix + file;
            File fileToRead = new File(fileToReadString);
            if (fileToRead.exists()) {
                LoadLog4J.LoadFromFile(fileToReadString);
                hasReadFile = true;
            }
        }
    }

    public void doGet(HttpServletRequest req, HttpServletResponse res) {
    }

    public void destroy() {

        log("Terminando Serlet" + getServletName());
    }

}
