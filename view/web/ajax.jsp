<%--
  Created by IntelliJ IDEA.
  User: spedison
  Date: 03/06/15
  Time: 18:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <style>
        .now {
            text-align: center;
            border-color: aqua;
        }

        .after {
            text-align: right;
            border-color: blue;
        }

    </style>

    <script src="./js/jquery-2.1.4.min.js"></script>
    <script>

        $(function () {

            function criaNovoJogo() {

                // Monta a URL para enviar as informações e efetuar a conversao
                var urlConverte = "./newgame.jsp?level=easy"

                // Pega o texto do item selecionado para adicionar no fim do item convertido.
                // var unidadeDestinoExtenso = $('#selectUnidadeDestino option:selected').html();

                // Envia a URL (Dados) em uma mensagem http para pegar o resultado.
                $.ajax({
                    type: "GET",
                    url: urlConverte,
                    contentType: "application/text; charset=utf-8",
                    dataType: "text",
                    success: function (data, status, jqXHR) {
                        alert(jqXHR.responseText);
                    }
                    ,
                    error: function (jqXHR, status) {
                        alert("Erro ao conectar com o WebServices....");
                    }
                });
            }


            $("#teste").click(function () {
                criaNovoJogo();
                $("div1").className("now");
            });
        })
        ;

    </script>
</head>
<body>

<div id="div1" class="after" ><h2>Let jQuery AJAX Change This Text</h2></div>

<button id="teste">Get External Content</button>

</body>
<script></script>
</html>
<!--
<script type="text/javascript" src="./js/Demonstracao.js"></script>
<script type="text/javascript" src="./js/TesteComum.js"></script>
<script type="text/javascript" src="./js/Retorno_Controles.js"></script>
<script type="text/javascript" src="./js/ajaxData.js"></script>


<link href="./css/style.css" rel='stylesheet' type='text/css' media="all"/>


< ! - -webfonts - - >
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,400,300,600,700,800'
rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Niconne' rel='stylesheet' type='text/css'>
< ! - - webfonts - - >
<link href="./css/component.css" rel="stylesheet" type="text/css"/>
-->