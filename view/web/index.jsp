<%@ page import="br.comsudoku.controler.vo.WebGameSession, br.com.sudoku.view.TableGrid" %>
<%@ page import="br.com.sudoku.view.LevelGameCombo" %>
<%@ page import="br.com.sudoku.view.KeyControl" %>
<%@ page import="br.comsudoku.controler.vo.WebGameControlSession" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page session="true" %>

<%--
  Created by IntelliJ IDEA.
  User: @GrupoAlpha
  Date: 03/06/15
  Time: 18:05
--%>
<%
    WebGameControlSession wgcs = WebGameControlSession.getSession();
    WebGameSession webGameSession = null;

    if (wgcs.containsKey(request.getSession().getId())) {
        // First step, get the session user
        webGameSession = wgcs.get(request.getSession().getId());
    }

    if (webGameSession == null)
        webGameSession = new WebGameSession();

    //session.setAttribute(WebGameSession.class.toString(), webGameSession);
    wgcs.put(request.getSession().getId(), webGameSession);

    String level = request.getParameter("level");

    if (level == null || level.isEmpty()) {
        level = "EASY";
    }

    webGameSession.makeNewGame(level.toUpperCase());

    TableGrid tableGrid = new TableGrid();
    tableGrid.setGameStrategy(webGameSession.getGameStrategy());
    KeyControl keyControl = new KeyControl();
    keyControl.setGameStrategy(webGameSession.getGameStrategy());
%>

<!DOCTYPE HTML>
<html>
<head>
    <title>Sudoku TCC</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Language" content="en-us">

    <meta name="msapplication-navbutton-color" content="#C40502">
    <meta name="description" content="IE9 Demonstracao">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <link rel="stylesheet" type="text/css" href="./css/Retorno_Controles.css">
    <link rel="stylesheet" type="text/css" href="./css/Demonstracao.css">

    <!-- bootstrap -->
    <link href="./css/bootstrap.css" rel='stylesheet' type='text/css' media="all"/>
    <!-- //bootstrap -->

    <link href="./css/style.css" rel='stylesheet' type='text/css' media="all"/>


    <!--webfonts-->
    <!--
    < l i n k  href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,400,300,600,700,800'
          rel='stylesheet' type='text/css'>
    < l i n k  href='http://fonts.googleapis.com/css?family=Niconne' rel='stylesheet' type='text/css'>
    < ! - - webfonts  -- -->

    <link href="./css/component.css" rel="stylesheet" type="text/css"/>
    <!-- Add jQuery -->
    <script src="./js/jquery-2.1.4.min.js" type="text/javascript"></script>

    <!-- jQuey in action -->
    <script type="text/javascript">

        $(document).ready(function () {

            var xhr = new XMLHttpRequest();
            $("#result").val("Iniciando o Jogo.");

            function processaComponente(line){

                var lineArr = line.split(";");

                if(lineArr.length != 3 ){
                    $("#result").val("Problema para setar um componente!");
                    alert("[" + line + "] Len = " + line.length);
                    return false;
                }

                $("#" + lineArr[0]).val(lineArr[1]);
                $("#" + lineArr[0]).prop('readOnly', (lineArr[2] == "1" ) ? false: true);

                return true;
            }

            function processReceivedData(data) {

                var dataArr = data.split("\n");

                /* alert("Recebido =[" + data + "]"); */


                for (var pos = 0; pos < dataArr.length; pos++) {

                    if(dataArr[pos].length == 0)
                        continue;

                    processaComponente(dataArr[pos]);
                }
            }

            <%=keyControl.writeKeyControlKeys("TB_USER", request.getSession().getId())%>

        });

    </script>


</head>
<body class="cbp-spmenu-push">
<form id="main_form" action="index.jsp" METHOD="get">
    <!--header-->
    <div class="header-section">
        <div class="container">
            <div class="header-top">
                <div class="header-logo">
                    <a href="index.jsp"><img src="./img/logo.png" alt=""/></a></div>
            </div>
            <div class="clearfix"></div>
            <div class="header-grids">
                <div class="col-md-6 header-grid-info">

                    <div class="header-grid gray">

                        <input type="hidden" id="exemploGrande" maxlength="2" value="@"  />

                        <%= tableGrid.writeTable("TB_USER") %>

                    </div>

                    <br>

                    <div class="row">

                        <div class="col-md-8 col-sm-offset-2 role=group btn-group header-grid-info">


                            <%= LevelGameCombo.makeCombo(level, "level", "level") %>

                            <br>

                            <div class="col-md-12 col-sm-offset-1 role= group btn-group header-grid-info">
                                <button type="submit" id="newgame" class="btn btn-primary">NOVO</button>
                            </div>
                        </div>
                    </div>

                    <br>
                </div>


                <div class="col-md-6 header-grid-info">

                    <div class="header-grid">


                    </div>


                    <br>


                    <div class="row">

                        <div class="col-md-8 col-sm-offset-2 role= group btn-group header-grid-info">

                            <br>

                            <div class="col-md-12 col-sm-offset-1 role= group btn-group header-grid-info">

                                <textarea id="result" cols="50" rows="20">
                                </textarea>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
</body>
</html>

