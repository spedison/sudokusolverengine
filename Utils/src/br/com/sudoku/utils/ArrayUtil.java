package br.com.sudoku.utils;

/**
 * File ArrayUtil.java created by @GrupoAlpha on 12/06/2015 at 20:00 for projetct SudokuSolverEngine.
 */
public class ArrayUtil {

    // Return position of element of array
    static public <T> int indexOf(T[] array, T item) {
        int ret = -1;
        int count = 0;
        for (T i : array) {
            if (i.equals(item)) {
                ret = count;
                break;
            }
            count++;
        }
        return ret;
    }

    static public int indexOf(int[] array, int item) {
        int ret = -1;
        int count = 0;
        for (int i : array) {
            if (i == item) {
                ret = count;
                break;
            }
            count++;
        }
        return ret;
    }

    static public int indexOf(long[] array, long item) {
        int ret = -1;
        int count = 0;
        for (long i : array) {
            if (i == item) {
                ret = count;
                break;
            }
            count++;
        }
        return ret;
    }


    static public boolean[] and(boolean[] p1, boolean[] p2) {
        if (p1.length != p2.length)
            throw new IndexOutOfBoundsException("p1 != p2");

        boolean[] ret = new boolean[p1.length];

        for (int x = 0; x < p1.length; x++)
            ret[x] = p1[x] & p2[x];

        return ret;
    }

    static public boolean[] or(boolean[] p1, boolean[] p2) {
        if (p1.length != p2.length)
            throw new IndexOutOfBoundsException("p1 != p2");

        boolean[] ret = new boolean[p1.length];

        for (int x = 0; x < p1.length; x++)
            ret[x] = p1[x] | p2[x];

        return ret;
    }


    static public boolean[] xor(boolean[] p1, boolean[] p2) {
        if (p1.length != p2.length)
            throw new IndexOutOfBoundsException("p1 != p2");

        boolean[] ret = new boolean[p1.length];

        for (int x = 0; x < p1.length; x++)
            ret[x] = p1[x] ^ p2[x];

        return ret;
    }

    static public boolean equal(boolean[] p1, boolean[] p2) {
        if (p1.length != p2.length)
            throw new IndexOutOfBoundsException("p1 != p2");

        boolean[] ret = new boolean[p1.length];

        for (int x = 0; x < p1.length; x++)
            if (p1[x] != p2[x])
                return false;

        return true;
    }

    static public int countTrue(boolean[] p) {

        int count = 0;

        for (boolean b : p) {

            if (b)
                count++;
        }

        return count;
    }

    static public int countFalse(boolean[] p) {

        int count = 0;

        for (boolean b : p) {

            if (!b)
                count++;
        }

        return count;
    }

}
