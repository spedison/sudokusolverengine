package br.com.sudoku.utils;

/**
 * Created by @GrupoAlpha on 03/04/15.
 */
public class ConfigEnviroment {

    public static String getHomeDir(){
        return System.getProperties().getProperty("user.home");
    }

    public static String getCurrentDir(){
        return System.getProperties().getProperty("user.dir");
    }
}
