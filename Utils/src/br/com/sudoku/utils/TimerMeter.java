package br.com.sudoku.utils;

/**
 * Created by @GrupoAlpha on 10/04/15.
 */
public class TimerMeter {

    // Tudo em milisegundos.
    private long tempoAcumulado;
    private long tempoInicialContagem;
    private long tempoFinalContagem;

    public TimerMeter() {
        reinicia();
    }

    public void reinicia() {
        tempoAcumulado = 0;
        tempoFinalContagem = 0;
        tempoInicialContagem = 0;
    }

    public void retomaContagem() {
        tempoInicialContagem = System.currentTimeMillis();
    }

    public void iniciaContagem() {
        tempoAcumulado = 0;
        tempoInicialContagem = System.currentTimeMillis();
    }

    public void paraContagem() {
        tempoFinalContagem = System.currentTimeMillis();
        tempoAcumulado += (tempoFinalContagem - tempoInicialContagem);
    }

    public long tempoAcumuladoMilisegundos() {
        return tempoAcumulado;
    }

    public double tempoAcumuladoEmHoras() {
        return (((double) tempoAcumulado) / 1000.0) / 3600.0;
    }

    public double tempoAcumuladoEmMinutos() {
        return (((double) tempoAcumulado) / 1000.0) / 60.0;
    }

    public double tempoAcumuladoEmSegundos() {
        return (((double) tempoAcumulado) / 1000.0);
    }

    public double tempoAcumuladoEmDias() {
        return (((double) tempoAcumulado) / 1000.0) / (3600.0 * 24);
    }
}
