package br.com.sudoku.utils;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.xml.DOMConfigurator;

import java.util.logging.Level;

/**
 * Created by @GrupoAlpha on 04/04/15.
 */
public class LoadLog4J {

    static public void LoadFromFile(String fileName){

        if(fileName.endsWith("properties"))
            PropertyConfigurator.configure(fileName);
        else if (fileName.endsWith("xml"))
            DOMConfigurator.configure(fileName);

        Logger logger = Logger.getLogger(LoadLog4J.class);
        logger.info("Log4J - Carregado....");


        java.util.logging.Logger.getLogger("org.hibernate").setLevel(Level.ALL);
    }

    static public void LoadFromApplication(String appName, String moduleName){
        String file = ConfigEnviroment.getHomeDir()+"/log/config/log4j-" + appName + "-" + moduleName + ".properties";
        LoadFromFile(file);
    }

}
